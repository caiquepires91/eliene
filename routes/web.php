<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use App\Http\Controllers\PlanoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NoticiaController;
use App\Http\Controllers\GalleryController;
use App\Models\Cidade;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:web,admin,aluno'])->group(function()
{
    Route::get('/dashboard', function()
    {
        return redirect('/');
    });

});

Route::post('cidades', function(Request $request) {
    $estado = $request->input("estado");
    $cidades = Cidade::where('estados_cod_estados', $estado)->get();
    return ['success'=>true, 'cidades'=>$cidades];
});

/** UserController */
Route::post('/send-message', [UserController::class, 'enviarMensagem'])->name('send-message');
Route::get('/logout', [UserController::class, 'logout'])->name('logout');
Route::post('/redefinir-senha', [UserController::class, 'redefinirSenha'])->name('redefinir-senha');
Route::get('/verificar-token/{token}', [UserController::class, 'verificarToken'])->name('verificar-token');
Route::post('/recuperar-senha', [UserController::class, 'recuperarSenha'])->name('recuperar-senha');
Route::post('/login', [UserController::class, 'authenticate'])->name('authenticate');
Route::get('/login', [UserController::class, 'goToLoginPage'])->name('login');
Route::post('/cadastrar', [UserController::class, 'cadastrar'])->name('cadastrar');

Route::resource('cursos', PlanoController::class);

Route::get('/eja', function(PlanoController $planoController) {
    return view('eja', ['page'=>'home-page', 'planos'=>$planoController->all()]);
});

Route::get('/contato', function(PlanoController $planoController) {
    return view('contato', ['page'=>'contato-page', 'planos'=>$planoController->all()]);
});

Route::get('/bg', function(){
    return view('common.bg');
});

Route::get('/autocomplete', [NoticiaController::class, 'autocomplete'])->name('autocomplete');
Route::get('/noticia/{id}', [NoticiaController::class, 'show'])->name('noticia');
Route::get('/noticias', [NoticiaController::class, 'index'])->name('noticias');

Route::get('/sobre', function(PlanoController $planoController) {
    return view('sobre', ['page'=>'sobre-page', 'planos'=>$planoController->all()]);
});

Route::get('/admin', function () {
    return redirect()->away("https://aulasremotas.lnconcursos.com.br/admin/home/login.php");
});

Route::get('/galeria/{id}', [GalleryController::class, 'show'])->name('galeria.show');
Route::get('/galeria', [GalleryController::class, 'index'])->name('galeria');

// Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/contato', function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/noticia/{id}',function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/noticias', function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/galeria/{id}',function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/galeria', function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/contatos', function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/cursos/{id}',function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/cursos', function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/sobre', function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/{url}', function($url) {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});
Route::get('/', function() {
    return redirect()->away('https://aulasremotas.lnconcursos.com.br/');
});