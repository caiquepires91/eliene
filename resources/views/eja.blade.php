@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<link href="{{url('css/eja.css')}}" rel="stylesheet">

<div class="section-eja mb-5">

    @include('common.top-bar', ['title'=>"EJA", 'subtitle'=>'Ensino Médio',])

    <div class="container">
        <div class="row">
            <div class="col">
               <div class="row">
                    <div class="col-md-6">
                        <div class="content hightlight-content-wrapper position-relative" style="background-image: url('/img/eja.webp')">
                            <div class="position-absolute text-wrapper d-flex align-items-center">
                            <p><div class="i-wrapper"><p><i class="fas fa-book"></i></p></div><span>EJA- Ensino Médio</span></p>
                            </div>  
                        </div>       
                    </div>
                    <div class="col-md-6">
                        <div class="content size-1">
                            <p><div class="i-wrapper"><p><i class="fas fa-info"></i></p></div><span>O que é o EJA-EAD?</span></p>
                            <hr>
                            <p>EAD é a sigla para Educação À Distância. É uma modalidade de ensino que dá mais flexibilidade e autonomia. Vem ganhando muito espaço por acompanhar as evoluções tecnológicas, a possibilidade de fazer seus próprios horários e com preços mais acessíveis que a modalidade de ensino presencial.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content size-1">
                            <p><div class="i-wrapper"><p><i class="fas fa-thumbs-up"></i></p></div><span>Quais são as vantagens da educação à distância?</span></p>
                            <hr>
                            <p>As principais vantagens da EAD são a flexibilidade de horários e a autonomia de estudos. Você pode dedicar-se nos horários que considerar mais adequados e conciliar melhor com outras atividades do cotidiano. Além disso, do ponto de vista financeiro os preços da educação a distância são mais acessíveis que a de escolas presenciais, além de diminuir custos diários com transporte, por exemplo.<p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content size-1">
                            <p><div class="i-wrapper"><p><i class="fas fa-users"></i></p></div><span>Quem pode matricular-se?</span></p>
                            <hr>
                            <p>Alunos de toda a região. Desde que obedeçam ao pré-requisito da idade: 18 anos completos para o Ensino Médio (antigo 2º grau). Não é obrigatório prévio histórico escolar.<p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content size-1">
                            <p><div class="i-wrapper"><p><i class="fas fa-globe-americas"></i></p></div><span>EJA EAD é mais fácil que o ensino presencial?</span></p>
                            <hr>
                            <p>Não. Assim como a EJA presencial, a modalidade a distância também tem provas, simulados e notas mínimas para receber o certificado de conclusão do ensino médio. Também exige tempo, dedicação e objetividade. No entanto, o EAD oferece mais liberdade para estudar nos seus próprios horários, tendo um ritmo diferente do tradicional.<p>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="content size-1">
                            <p><div class="i-wrapper"><p><i class="fas fa-compress-alt"></i></p></div><span>Qual é a relação entre curso supletivo e EJA?</span></p>
                            <hr>
                            <p>O curso supletivo se refere a uma nomenclatura antiga e deixou de existir com esse nome, agora se chama Curso de Educação para Jovens e Adultos (EJA).<p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content size-1">
                            <p><div class="i-wrapper"><p><i class="fas fa-cogs"></i></p></div><span>Como funciona?</span></p>
                            <hr>
                            <p>O nosso processo de ensino é simplificado e 100% a distância, podendo ser concluído em até 6 meses!<p>
                            <p><i class="fas fa-chevron-right"></i> São 3 módulos.</p>
                            <p><i class="fas fa-chevron-right"></i> 11 Matérias cada módulo.</p>
                            <p><i class="fas fa-chevron-right"></i> Provas de múltipla escolha.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content size-1">
                            <p><div class="i-wrapper"><p><i class="fas fa-th-list"></i></p></div><span>Qual a modalidade da prova do EJA EAD?</span></p>
                            <hr>
                            <p>Temos duas modalidades: Online (Plataforma online + prova online na unidade) e Presencial (Prova objetiva na unidade).</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content">
                            <p><div class="i-wrapper"><p><i class="fas fa-not-equal"></i></p></div><span>Qual a diferença das duas modalidades?</span></p>
                            <hr>
                            <p><i class="fas fa-chevron-right"></i> Na modalidade online, você tem acesso a uma plataforma com aulas online para ajudar na sua preparação durante o EJA e também do auxílio de um roteiro de estudos em PDF. Mesmo sendo escolhida a modalidade da prova online, a prova será realizada na unidade e com horário marcado e com a prova digital.</p>
                            <p><i class="fas fa-chevron-right"></i> Na modalidade presencial, você tem acesso a um roteiro de estudos em PDF para ajudar na sua preparação durante o EJA. A prova será realizada na unidade e com horário marcado e com a prova objetiva.</p>
                        </div>
                    </div>    
               </div>
            </div>
        </div>
    </div>
</div>
@include('common.footer')
@include('build.scripts')
@stop

