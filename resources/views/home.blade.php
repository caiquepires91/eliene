@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<link href="{{url('css/home.css')}}" rel="stylesheet">

<body>

    @include('common.common')

    <!-- Modal - Esqueci a senha -->
    <div class="modal theme-primary-modal fade" id="recuperarSenhaModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="form-recuperar-senha" class="needs-validation" action="{{url('recuperar-senha')}}" novalidate>
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Redefinir Senha</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                    @csrf
                    <label for="basic-url" class="form-label">Um link será enviada pra esse endereço de e-mail</label>
                    <div class="input-group mb-3 has-validation position-relative">
                        <span class="input-group-text" id="email-label"><i class="fas fa-envelope-open-text"></i></span>
                        <input type="text" class="form-control" placeholder="E-mail" name="email" aria-label="Email" aria-describedby="email-label;email-help;">
                        <div class="invalid-tooltip" >
                            Esse campo é obrigatório.
                        </div>
                    </div>
                    <p class="feedback" style="display:none">Enviamos um link para o e-mail:</p>
            </div>
            <div class="modal-footer pb-5">
                <button type="submit" class="btn theme-primary-btn">Redefinir</button>
            </div>
            </form>
        </div>
    </div>
    </div>

    <!-- Modal - Cadastro -->
    <div class="modal fade" id="modalCadastro" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalCadastro" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
           <div class="modal-content-subwrapper">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCadastro"></h5>
                    <button type="button" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 modal-inputs-wrapper">
                            <div class="row justify-content-between align-items-end mb-3">
                                <div class="col-md-8">
                                    <p class="title">Oi!</p>
                                    <p class="title"><strong>faça seu <span class="theme-primary-color">cadastro</span></strong></p>
                                </div>
                                <div class="col-md-4">
                                    <div class="d-flex justify-content-end brand">
                                        <a href="{{url('/')}}" ><img src="{{url('img/main-logo.svg')}}" alt="eliene cursos e concursos"/></a>
                                    </div>
                                </div>
                            </div>
                            <form class="needs-validation" id="form-cadastro" action="{{url('cadastrar')}}" novalidate>
                                @csrf
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="name-input"><i class="fas fa-user"></i></span>
                                    <input type="text" class="form-control" name="nome" placeholder="Nome completo" aria-label="Name" aria-describedby="name-input">
                                    <div class="invalid-tooltip " >
                                        Esse campo é obrigatório.
                                    </div>
                                </div>
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="email-input"><i class="fas fa-envelope-open-text"></i></span>
                                    <input type="email" class="form-control" name="email" placeholder="E-mail" name="email" aria-label="Email" aria-describedby="email-input">
                                    <div class="invalid-tooltip" >
                                        Esse campo é obrigatório.
                                    </div>
                                </div>
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="cpf-input"><i class="fas fa-id-card"></i></span>
                                    <input type="text" class="form-control" name="cpf" placeholder="CPF" aria-label="cpf" aria-describedby="cpf-input">
                                    <div class="invalid-tooltip" >
                                        Esse campo é obrigatório.
                                    </div>
                                </div>
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="nascimento-input"><i class="fas fa-calendar-day"></i></span>
                                    <input type="text" class="form-control date" name="nascimento" placeholder="Data de nascimento" aria-label="cpf" aria-describedby="nascimento-input">
                                    <div class="invalid-tooltip" >
                                        Por favor, digite uma data válida.
                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <select class="form-select has-validation position-relative" name="estado" aria-label="selecionar estado">
                                        <option class="theme-primary-color">Estado</option>
                                        @isset ($estados)
                                            @foreach ($estados as $key=>$estado)
                                                <option value="{{$estado->cod_estados}}">{{$estado->nome}}</option>
                                            @endforeach 
                                        @endisset
                                    </select>
                                    <div class="invalid-tooltip" >
                                        Esse campo é obrigatório.
                                    </div>
                                    <select class="form-select has-validation position-relative" name="cidade" aria-label="selecionar cidade" disabled>
                                        <option selected class="theme-primary-color">Cidade</option>
                                    </select>
                                    <div class="invalid-tooltip" >
                                        Esse campo é obrigatório.
                                    </div>
                                </div>
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="cep-input"><i class="fas fa-street-view"></i></span>
                                    <input type="text" class="form-control" name="cep" placeholder="CEP" aria-label="Cep" aria-describedby="cep-input">
                                    <div class="invalid-tooltip" >
                                        Esse campo é obrigatório.
                                    </div>
                                </div>
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="phone-input"><i class="fas fa-phone"></i></span>
                                    <input type="tel" class="form-control" name="telefone" placeholder="Telefone" aria-label="Telefone" aria-describedby="phone-input">
                                    <div class="invalid-tooltip" >
                                        Esse campo é obrigatório.
                                    </div>
                                </div>
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="password-input"><i class="fas fa-key"></i></span>
                                    <input type="password" autocomplete="new-password" class="form-control" name="senha" placeholder="Senha" aria-label="password" aria-describedby="password-input">
                                    <span class="input-group-text" id="password-input"><i class="fas fa-check-double"></i></span>
                                    <input type="password" autocomplete="new-password" class="form-control" name="confirma-senha" placeholder="Confirmar Senha" aria-label="password" aria-describedby="password-input">
                                    <div class="invalid-tooltip" >
                                        As senhas não combinam.
                                    </div>
                                </div>
                                <div class="position-relative mb-3 has-validation position-relative">
                                    <input class="form-check-input" type="checkbox" name="termo" value="" id="cadastrar-checkbox" aria-describedby="cadastrar-check-fb">
                                    <label class="form-check-label" for="cadastrar-check" style="line-height: 30px;">
                                        Li e aceito os <a href="{{url('termo-uso')}}" target="_blank" >termos de utilização</a>.
                                    </label>
                                    <div class="invalid-tooltip">
                                        Leia os termos de utilização antes de continuar.
                                    </div>
                                </div>
                                <div class="mb-3 d-flex justify-content-center">
                                    <button type="submit" class="btn theme-primary-btn">Cadastrar</button>
                                </div>
                            </form>                      
                        </div>
                        <div class="col-md-6 modal-body-bg">
                            <p id="cadastro-feedback">Ops! Algo deu errado.</p>
                            <img src="{{url('/img/cadastro-bg.webp')}}" alt="eliene concursos - background cadastro">
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-start">
                </div>
           </div>
        </div>
        </div>
    </div>

    <div class="main-banner" data-url="{{url('/')}}">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
            </div>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="{{url('img/banner-1.webp')}}" class="d-block w-100" alt="banner o seu futuro começa aqui">
              </div>
              <div class="carousel-item">
                <img src="{{url('img/banner-1.webp')}}" class="d-block w-100" alt="banner o seu futuro começa aqui">
              </div>
            </div>
            <button class="carousel-control-prev theme-secondary-indicator" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next theme-secondary-indicator" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
    </div>

    <div class="theme-secondary-bg">
        <div class="container">
            <div class="row justify-content-center align-items-center curso-info-bar">
                <div class="col">
                    <p class="title">+10</p>
                    <p class="subtitle">anos</p>
                </div>
                <div class="col">
                    <p class="title">+400</p>
                    <p class="subtitle">aprovações</p>
                </div>
                <div class="col">
                    <p class="title">+20</p>
                    <p class="subtitle">cursos</p>
                </div>
            </div>
        </div>
    </div>

    <div class="section-quem-somos mt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="section-title">Quem Somos</p>
                    <div class="row">
                        <div class="col-md-8">
                            <p class="section-subtitle">Nossa missão é a sua aprovação</p>
                            <p class="section-text">O LN Cursos e Concursos surgiu em 2009, com o ideal de preparar os alunos da região para os concursos públicos da região nordeste, tendo registrado altíssimas taxas de crescimento nos últimos anos. Nosso propósito é transformar vidas por meio da educação. A empresa surgiu de uma iniciativa da empresária Eliene Moura que, soma mais de 20 anos de experiência no mercado de concursos públicos e pré-vestibular. O objetivo é democratizar o ensino de uma maneira nunca vista na nossa região, oferecendo ferramentas inovadoras e conteúdo educacional de excelência para todos que buscam um futuro melhor.</p>
                            <p class="section-text">O LN Cursos e Concursos oferece cursos nas modalidades online e presencial, apresentando mais comodidade, economia e ganho de tempo aos concurseiros que buscam um ensino de qualidade...</p>
                            <a class="theme-primary-btn btn mt-3" role="button" href="{{url('/sobre')}}" >Saiba mais</a>
                        </div>
                        <div class="col-md-4 d-md-block d-block justify-content-center position-relative">
                            <img id="slider" src="{{url('img/cartoon.webp')}}" alt="eliene - professora cartoon">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link href="{{url('css/flip.css')}}" rel="stylesheet">

    <div class="section-diferenciais mt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="section-title">Diferenciais</p>
                    <p class="section-subtitle">Concursos públicos e vestibulares</p>
                    <div class="row justify-content-center mb-2">
                        <div class="col-lg-4 col-xl-3 column">
                            <div class="diferenciais-card menu__container">
                                <div class="card-image">
                                    <img src="{{url('img/resolucao-icon.svg')}}" alt="resolução de questões">
                                </div>
                                <div class="card-title">Resolução de questões</div>
                                <div class="card-subtitle">Técnica que serve para estudar para aquele concurso que você tanto sonha, trazendo vídeo aulas focadas apenas nas melhores bancas de concursos, trazendo-lhe um raciocínio rápido e lógico.</div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-3 column">
                            <div class="diferenciais-card menu__container">
                                <div class="card-image">
                                    <img src="{{url('img/videoaulas-icon.svg')}}" alt="videoaulas online">
                                </div>
                                <div class="card-title">Plataforma de videoaulas online</div>
                                <div class="card-subtitle">Já imaginou estudar no conforto da sua casa? Ou em qualquer outro lugar? Pensando desta forma nós do LN cursos e concursos criamos um conteúdo exclusivo para você.</div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-3 column">
                            <div class="diferenciais-card menu__container">
                                <div class="card-image">
                                    <img src="{{url('img/conteudo-icon.svg')}}" alt="conteúdo estratégico e atualizado">
                                </div>
                                <div class="card-title">Conteúdo estratégico e atualizado</div>
                                <div class="card-subtitle">Nós do LN cursos e concursos pensamos em você. Trazendo os melhores e mais capacitados professores, nos mais variados concursos públicos e vestibulares, para facilitar e sanar todas as suas dúvidas.
                                    </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-3 column">
                            <div class="diferenciais-card menu__container">
                                <div class="card-image">
                                    <img src="{{url('img/equipe-icon.svg')}}" alt="equipe qualificada">
                                </div>
                                <div class="card-title">Equipe qualificada</div>
                                <div class="card-subtitle">Nós também contamos com aulas presenciais em nosso polo, as turmas presenciais se reúnem em dias e horários específicos, a depender do curso.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-unicesumar">
        <div class="container d-flex justify-content-center align-items-center">
            <div class="row unicesumar-row">
                <div class="col-md-3 d-flex justify-content-center align-items-center">
                   <img src="{{url('img/unicesumar.png')}}">
                </div>
                <div class="middle-content col-md-6 d-flex justify-content-center align-items-center">
                   <div>
                       <p class="theme-secondary-color">EDUCAÇÃO</p>
                       <p class="theme-primary-color">PRESENCIAL</p>
                   </div>
                   <div class="ms-3">
                       <p class="theme-secondary-color">METODOLOGIA</p>
                       <p class="theme-primary-color">HÍBRIDA</p>
                   </div>
                   <div class="ms-3">
                       <p class="theme-secondary-color">EDUCAÇÃO</p>
                       <p class="theme-primary-color">A DISTÂNCIA</p>
                   </div>
                </div>
                <div class="col-md-3 d-flex justify-content-center align-items-center">
                    <a class="theme-secondary-btn btn" role="button" href="https://www.unicesumar.edu.br" target="_blank" >Conheça</a>
                </div>
            </div>
        </div>
    </div>

    <div class="section-galeria mt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="section-title">Galeria</p>
                    <div class="row justify-content-between">
                        <div class="col">
                            <p class="section-subtitle">Fotos eventos</p>
                        </div>
                        <div class="col">
                            <div class="d-flex flex-row-reverse">
                                <a class="theme-outline-btn btn" role="button" href="{{url('galeria')}}" >Saiba mais</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    @isset($albums)
                    @foreach($albums as $album)
                        <div class="col-lg-4 col-xl-3">
                            <div class="galeria-card position-relative" data-id="{{$album->id}}">
                                <div class="img" style="background-image: url('/img/{{$album->src}}')" alt="album {{$album->titulo}}"></div>
                                <div class="album-mask position-absolute d-flex align-items-center">
                                    <p>{{$album->titulo}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="section-acesso" class="section-acesso mt-5">
        <div class="container">
            <div class="borda-topo row"></div>
            <div class="row p-5">
                <div class="col-md-6 mb-2">
                    <p class="section-subtitle">Já é aluno? Acesse a plataforma</p>
                    <div class="row">
                        @php
                            $loggedIn = false;
                            $rota = 'main.php';
                            $admin = session('admin');
                            if ($admin) {
                                $userName = $admin->nome;
                                $loggedIn = true;
                                $rota = 'main_admin.php';
                            }
                            if (Auth::guard('admin')->check()) {
                                $userName = Auth::guard('admin')->user()->nome;
                                $loggedIn = true;
                                $rota = 'main_admin.php';
                            }
                            else if (Auth::guard('web')->check()) {
                                $userName = Auth::guard('web')->user()->name;
                                $loggedIn = true;
                            }
                        @endphp
                        @if (!$loggedIn)
                        <div class="col">
                            <form class="needs-validation" id="form-home-acesso" action="{{url('login')}}" novalidation>
                                @csrf
                                <!-- <label for="input-acesso-email" class="form-label">Email</label>
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="emailHelp"><i class="fas fa-envelope"></i></span>
                                    <input type="email" class="form-control" id="input-acesso-email" name="email" aria-describedby="emailHelp">
                                    <div class="invalid-tooltip " >
                                        Esse campo é obrigatório.
                                    </div>
                                </div> -->
                                <label for="input-acesso-cpf" class="form-label">CPF</label>
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="cpfHelp"><i class="fas fa-id-card-alt"></i></span>
                                    <input type="cpf" class="form-control" id="input-acesso-email" name="cpf" aria-describedby="cpfHelp">
                                    <div class="invalid-tooltip " >
                                        Esse campo é obrigatório.
                                    </div>
                                </div>
                                <label for="input-acesso-senha" class="form-label">Senha</label>
                                <div class="input-group mb-3 has-validation position-relative">
                                    <span class="input-group-text" id="senhaHelp"><i class="fas fa-key"></i></span>
                                    <input type="password" class="form-control" id="input-acesso-senha" name="password" aria-describedby="senhaHelp">
                                    <div class="invalid-tooltip" >
                                        Esse campo é obrigatório.
                                    </div>
                                </div>
                                <a class="link-warning me-3" data-bs-toggle="modal" data-bs-target="#recuperarSenhaModal">Esqueci minha senha</a>
                                <button type="submit" class="btn theme-primary-btn">Entrar</button>
                            </form>
                        </div>
                        @endif
                        <div class="col d-flex align-items-end">
                            <div class="my-auto">
                                <div class="mt-auto mb-3">
                                    <p><i class="fas fa-check theme-secondary-color"></i> Videoaulas gravadas e ao vivo</p>
                                    <p><i class="fas fa-check theme-secondary-color"></i> Simulados</p>
                                    <p><i class="fas fa-check theme-secondary-color"></i> Tira dúvidas</p>
                                    <p><i class="fas fa-check theme-secondary-color"></i> Material complementar</p>
                                </div>

                                @if (!$loggedIn)
                                <a class="link-warning" type="button" data-bs-toggle="modal" data-bs-target="#modalCadastro"><p>Ainda não possuo cadastro</p></a>
                                @else
                                <a class="theme-secondary-alt-btn btn" role="button" href="{{url('https://aulasremotas.lnconcursos.com.br/aluno/' . $rota )}}"><i class="fas fa-tv"></i> Ambiente do Aluno</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
            <div class="borda-fundo row"></div>
        </div>
    </div>

    <div class="section-matricula mt-5 py-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="section-title">Matricule-se</p>
                    <div class="row justify-content-between">
                        <div class="col">
                            <p class="section-subtitle">Nossos cursos</p>
                        </div>
                        <div class="col">
                            <div class="d-flex flex-row-reverse">
                                <a class="theme-outline-btn btn" role="button" href="{{url('/cursos')}}" >Ver todos</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @isset($planos)
                        @foreach($planos as $key=>$plano)
                      <!--   <div class="col-lg-6 col-xl-4 mb-3 d-flex justify-content-center flip-box">
                            <div class="front theme-primary-bg">
                              <div class="content matricula-card">
                                <div class="header" style="background-image:url('/img/{{$plano->img}}');"></div>
                                <div class="p-3">
                                    <div class="text">
                                        <p class="title">{{$plano->nome}}</p>
                                        <p class="subtitle">{{$plano->resumo}}</p>
                                    </div>
                                    <div class="row mt-3 info">
                                        <div class="col-6">
                                            <p><i class="fas fa-user-friends"></i> 50 vagas</p>
                                            <p><i class="fas fa-calendar-alt"></i> Início em {{date('d/m/Y', strtotime($plano->inicio))}}</p>
                                        </div>
                                        <div class="col-6">
                                            <p><i class="fas fa-donate"></i> 12X <span class="money">{{round($plano->valor/12,2)}}<span></p>
                                            <p><i class="fas fa-clock"></i> 120h/aula</p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-6">
                                            <ul class="users-list">
                                                <li style="background:url('img/logo.png')"></li>
                                                <li style="background:url('img/logo.png')"></li>
                                                <li style="background:url('img/logo.png')"></li>
                                                <li style="background:url('img/logo.png')"></li>
                                            </ul>
                                        </div>
                                        <div class="col-6">
                                            <a class="theme-secondary-square-btn  btn" role="button" href="{{url('cursos/'.$plano->id)}}" >Saiba mais</a>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="back">
                              <div class="content" >
                                <div class="d-flex justify-content-center align-items-center matricula-card h-100" style="background-image:url('/img/{{$plano->img}}'); flex-direction: column;" >
                                    <h2 style="color: white;font-family: 'Arial Rounded MT';font-weight: bolder;text-shadow: -4px 2px 4px black;">{{$plano->nome}}</h2>
                                    <div class="d-flex justify-content-center">
                                        <a class="theme-light-btn btn" role="button" href="{{url('cursos/'. $plano->id)}}">Saiba mais</a>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>
 -->
                        <div class="col-lg-6 col-xl-4 mb-3 d-flex justify-content-center">
                            <div class="matricula-card">
                                <div class="header" style="background-image: url('/img/{{$plano->img}}');"></div>
                                <div class="p-3">
                                    <div class="text">
                                        <p class="title">{{$plano->nome}}</p>
                                        <p class="subtitle">{{$plano->resumo}}</p>
                                    </div>
                                    <div class="row mt-3 info">
                                        <div class="col-6">
                                            <p><i class="fas fa-user-friends"></i> 50 vagas</p>
                                            <p><i class="fas fa-calendar-alt"></i> Início imediato</p>
                                        </div>
                                        <div class="col-6">
                                            <p><i class="fas fa-donate"></i>   @if ($plano->id == 7) 6X @elseif ($plano->id == 9) 1X @else 12X @endif<span class="money">
                                            @if ($plano->id == 4)62,00
                                            @elseif ($plano->id == 6)136,00
                                            @elseif ($plano->id == 7)64,45
                                            @elseif ($plano->id == 9)300,00
                                            @else {{floatval(round($plano->valor/12,2))}}
                                            @endif 
                                            <span></p>
                                            <p><i class="fas fa-clock"></i> 120h/aula</p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-6">
                                            <ul class="users-list" style="display:none;">
                                                <li style="background:url('img/logo.png')"></li>
                                                <li style="background:url('img/logo.png')"></li>
                                                <li style="background:url('img/logo.png')"></li>
                                                <li style="background:url('img/logo.png')"></li>
                                            </ul>
                                        </div>
                                        <div class="col-12 d-flex justify-content-center">
                                            <a class="theme-secondary-square-btn  btn" role="button" href="{{url('cursos/'.$plano->id)}}" >Saiba mais</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="section-aprovados mt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="mb-3 position-relative title-wrapper">
                        <p class="section-subtitle position-absolute">Aprovados!</p>
                        <hr class="tilt-line position-absolute">
                    </div>
                    <div id="carousel-aprovados-indicators" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            @isset( $depoimentos )
                            @foreach( $depoimentos as $key=>$depoimento )
                            <button type="button" data-bs-target="#carousel-aprovados-indicators" data-bs-slide-to="{{$key}}" class="{{ $key ? '': 'active'}}" aria-current="true" aria-label="Slide {{$depoimento->id}}"></button>
                            @endforeach
                            @endisset
                        </div>
                        <div class="carousel-inner">
                        @isset( $depoimentos )
                        @foreach( $depoimentos as $key=>$depoimento )
                          <div class="carousel-item {{ $key ? '': 'active'}}">
                            <div class="row aprovados-slide">
                                <div class="col-md-4">
                                    <div class="image-wrapper d-flex justify-content-center position-relative">
                                        <div class="square" style="background-image: url('/img/depoimentos/{{$depoimento->img}}');">
                                        </div>
                                        <div class="name-wrapper position-absolute">
                                            <div class="name">
                                                <p>{{strtoupper($depoimento->autor)}}</p>
                                            </div>
                                            <hr class="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 d-flex">
                                    <div class="quote-left d-md-flex d-none align-items-start"><p><i class="fas fa-quote-left"></i></p></div>
                                    <div class="text-wrapper">
                                        <p class="depoimento">{{$depoimento->depoimento}}</p>
                                        <p class="curso">{{$depoimento->cursos}}</p>
                                    </div>
                                    <div class="quote-right d-md-flex d-none align-items-end"><p><i class="fas fa-quote-right"></i></p></div>
                                </div>
                            </div>
                          </div>
                        @endforeach
                        @endisset
                        </div>
                       <!--  <button class="carousel-control-prev" type="button" data-bs-target="#carousel-aprovados-indicators" data-bs-slide="prev">
                            <i class="fas fa-chevron-left"></i>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carousel-aprovados-indicators" data-bs-slide="next">
                            <i class="fas fa-chevron-right"></i>
                        </button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-cursos mt-5 d-flex align-items-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-9">
                   <p>Seja um dos nossos aprovados! Matricule-se!</p>
                </div>
                <div class="col-md-3">
                    <div class="d-flex justify-content-center">
                        <a class="theme-light-btn btn" role="button" href="{{url('cursos')}}" >Ver cursos<i class="fas fa-arrow-right ms-3"></i></a>
                    </div>
                 </div>
            </div>
        </div>
    </div>

    <div class="section-unicesumar">
        <div class="container d-flex justify-content-center align-items-center">
            <div class="row unicesumar-row">
                <div class="col-md-3 d-flex justify-content-center align-items-center">
                   <img src="{{url('img/unicesumar.png')}}">
                </div>
                <div class="middle-content col-md-6 d-flex justify-content-center align-items-center">
                   <div>
                       <p class="theme-secondary-color">EDUCAÇÃO</p>
                       <p class="theme-primary-color">PRESENCIAL</p>
                   </div>
                   <div class="ms-3">
                       <p class="theme-secondary-color">METODOLOGIA</p>
                       <p class="theme-primary-color">HÍBRIDA</p>
                   </div>
                   <div class="ms-3">
                       <p class="theme-secondary-color">EDUCAÇÃO</p>
                       <p class="theme-primary-color">A DISTÂNCIA</p>
                   </div>
                </div>
                <div class="col-md-3 d-flex justify-content-center align-items-center">
                    <a class="theme-secondary-btn btn" role="button" href="https://www.unicesumar.edu.br" target="_blank">Conheça</a>
                </div>
            </div>
        </div>
    </div>

    <div class="section-noticias mt-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="section-title">Novidades</p>
                    <div class="row justify-content-between">
                        <div class="col">
                            <p class="section-subtitle">Últimas notícias</p>
                        </div>
                    </div>
                    <div class="row">
                        @isset($noticias)
                        @foreach($noticias as $key=>$noticia)
                        <div class="col-lg-6 col-xl-4 mb-2 d-flex justify-content-center">
                            <div class="noticias-card">
                                <div class="header" style="background-image: url('https://aulasremotas.lnconcursos.com.br/admin/home/arquivos/{{$noticia->img}}');"></div>
                                <div class="p-1">
                                    <div class="text m-3">
                                        <p class="title">{{mb_strimwidth($noticia->titulo, 0, 70, "...")}}</p>
                                        <p class="content">{{strip_tags(mb_strimwidth(base64_decode($noticia->noticia), 0, 200, "..."))}}</p>
                                    </div>
                                    <div class="row p-3 noticias-rodape">
                                        <div class="col-6 theme-primary-color d-flex align-items-center">
                                            <p><i class="fas fa-calendar-alt"></i> <i>postado em {{date("d/m/Y", strtotime($noticia->data_publicacao))}}</i></p>
                                        </div>
                                        <div class="col-6">
                                            <a class="theme-primary-btn  btn" role="button" href="{{url('noticia') . '/' . $noticia->id}}" target="_blank">Leia mais</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endisset
                    </div>
                    <div class="d-flex justify-content-end mt-5">
                        <a class="theme-outline-btn btn" role="button" href="{{url('/noticias')}}" >Ver todas as notícias</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-maps mt-5">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3936.25267605029!2d-38.22796858460297!3d-9.399209400862086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x70930b04ea864dd%3A0xc6a499dd8b9a94d!2sLN%20Cursos%20e%20Concursos!5e0!3m2!1sen!2sbr!4v1618839237543!5m2!1sen!2sbr" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>

</body>

@include('common.footer')
@include('build.scripts')

<script src="{{mix('js/home.js')}}"></script>

@stop


