@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<link href="{{url('css/album.css')}}" rel="stylesheet">

<div class="section-galeria mb-5">

    @include('common.top-bar', ['title'=>"Galeria", 'subtitle'=>'Fotos Eventos'])

    <div class="container">
        <nav class="mb-5"  aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/galeria')}}">Galeria</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$album->titulo}}</li>
            </ol>
        </nav>
        <div class="galeria" id="gallery"  data-url="{{url('/')}}">
            @php
                $big = false;
            @endphp
            @isset($pics)
            @foreach( $pics as $key=>$pic )
                @php
                    $rand = rand(0,100);
                    $big = false;
                    if ($rand > 70 && $key%2 == 0) {
                        $big = true;
                    } 
                @endphp
                <div class="galeria-item" style="{{ $big ? 'grid-row-end: span 30' : 'grid-row-end: span 15'}}" data-id="{{$pic->id}}">
                    <div class="galeria-pasta content">
                        <img class="img" src="{{url('/img') . '/' . $pic->src}}" alt="pasta galeria">
                    </div>
                </div>
            @endforeach
            @endisset
        </div>
    </div>
</div>
@include('common.footer')
@include('build.scripts')

<script src="{{mix('js/album.js')}}"></script>

@stop

