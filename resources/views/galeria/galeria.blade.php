@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<link href="{{url('css/galeria.css')}}" rel="stylesheet">

<div class="section-galeria mb-5">

    @include('common.top-bar', ['title'=>"Galeria", 'subtitle'=>'Fotos Eventos'])

    <div class="container">
        <div class="galeria" id="gallery" data-url="{{url('/')}}">
            @php
            $big = false;
            @endphp
            @isset($albums)
            @foreach( $albums as $key=>$album )
                @php
                    $rand = rand(0,100);
                    $big = false;
                    if ($rand > 70 && $key%2 == 0) {
                        $big = true;
                    } 
                @endphp
                <div class="galeria-item" style="{{ $big ? 'grid-row-end: span 30' : 'grid-row-end: span 15'}}" data-id="{{$album->id}}">
                    <div class="galeria-pasta position-relative content">
                        <div class="img" style="background-image: url('/img/{{$album->src}}')" alt="pasta galeria"></div>
                        <div class="album-mask position-absolute d-flex align-items-center">
                            <p>{{$album->titulo}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
            @endisset
          <!-- <div class="galeria-item" style='grid-row-end: span 15' data-id="1">
                <div class="galeria-pasta">
                    <div class="img" style="background-image: url('/img/plano-enem-gold.webp')" alt="pasta galeria"></div>
                </div>
            </div> -->
        </div>
    </div>
</div>
@include('common.footer')
@include('build.scripts')

<script src="{{mix('js/galeria.js')}}"></script>

@stop

