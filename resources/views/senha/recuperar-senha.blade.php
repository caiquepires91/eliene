@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<style>
.recuperar-senha-box .warning-text {
    color: green;
    text-align: center;
}
</style>
<div class="container">
    <div  class="row">
        <div class="col">
            <p class="section-title">Senha</p>
            <p class="section-subtitle">Redefinir Senha</p>
        </div>
    </div>
    <div class="row justify-content-center my-5">
        <div class="recuperar-senha-box" style="width:400px">
            <form id="form-recuperar-senha" action="{{url('redefinir-senha')}}" name="form_recuperar_senha">
                @csrf
                <div class="mb-3 has-validation position-relative">
                    <label for="nova-senha" class="form-label" id="label-nova-senha"><strong>Nova senha*:</strong></label>
                    <input type="password" name="nova_senha"  class="form-control" autocomplete="new-password" id="nova-senha" aria-describedby="label-nova-senha">
                    <div class="invalid-tooltip">
                        Por favor, digite uma senha válida.
                    </div>
                </div>
                <div class="mb-3 has-validation position-relative">
                    <label for="confirmar-nova-senha" class="form-label" id="label-confirmar"><strong>Confirmar nova senha*:</strong></label>
                    <input type="password" name="confirmar_nova_senha" autocomplete="new-password" class="form-control" id="confirmar-nova-senha" aria-describedby="label-confirmar">
                    <div class="invalid-tooltip">
                        As senhas não combinam.
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn theme-primary-btn">Redefinir senha</button>
                </div>
                <div id="warning-box" class="align-self-center mt-3" style="display: none;">
                    <p class="warning-text"></p>
                </div>
            </form>
        </div>
    </div>
</div>

@include('common.footer')
@include('build.scripts')

<script>
    $("#form-recuperar-senha").submit(function(e){
        e.preventDefault();
        e.stopPropagation();

        $("#form-recuperar-senha button").html("aguarde...");

        var url = $(this).attr('action');

        var senha = $("#nova-senha");
        var confirmaSenha = $("#confirmar-nova-senha");

        var cancel = false;
        var input = null;

        if (senha.val() != confirmaSenha.val()) {
                cancel = true;
                input = confirmaSenha;
            }

        if (senha.val() == "") {
            cancel = true;
            input = senha;
        }

        var formSerialized = $(this).serializeArray();
        formSerialized.push({name: 'token', value: "{{$token}}"});
           
        if (cancel) {
            input.addClass("is-invalid");
            input.focus();
            $("#form-recuperar-senha button").html("Redefinir senha");
        } else {
            $.post($(this).attr('action'), formSerialized, function(data) {
                console.log("data: " + data.msg);
                if (data.success) {
                    $(".warning-text").css('color', 'green');
                    $(".warning-text").html(data.msg);
                    $("#warning-box").fadeIn();
                } else {
                    $(".warning-text").css('color', 'red');
                    $(".warning-text").html(data.msg);
                    $("#warning-box").fadeIn();
                    //console.log("cadastro falhou!!");
                }
            })
            .fail(function() {
                $(".warning-text").css('color', 'red');
                $(".warning-text").html("Redefinição de senha falhou! Por favor, tente mais tarde.");
                $("#warning-box").fadeIn();
                //console.log("cadastro falhou!!")
            })
            .always(function(){
                $("#form-recuperar-senha button").html("Redefinir senha");
            });
        }

    });
</script>