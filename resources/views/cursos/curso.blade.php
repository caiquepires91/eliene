@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<link href="{{url('css/curso.css')}}" rel="stylesheet">

<div class="section-curso mb-5" data-plano="{{$plano}}">

    @include('common.top-bar', ['title'=>"Curso", 'subtitle'=>$plano->nome,
    'leftContent'=> htmlentities('<div class="d-flex flex-row-reverse w-100"><div class="me-3"><a class="theme-light-btn btn" role="button" target="_blank"
    href="https://api.whatsapp.com/send?phone=5575991125290&text=Ol%C3%A1,%20gostaria%20de%20contratar%20um%20dos%20cursos%20do%20Eliene%20Cursos%20e%20Concursos." >Contratar</a></div></div>')
    ])

    <div class="container">
        <div class="row">
            <div class="col">
                <nav class="mb-4"  aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('cursos')}}">Todos</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$plano->nome}}</li>
                    </ol>
                </nav>
                <!-- <p class="section-title">Curso</p> -->
                <div class="row justify-content-between">
                    <div class="col-md-6 col-6">
                        <!-- <p class="section-subtitle">{{$plano->nome}}</p> -->
                    </div>
                    <div class="col-md-2 col-6 d-flex flex-row-reverse">
                        <!-- <div class="me-3"><a class="theme-primary-btn btn" role="button" href="#" >Contratar</a></div> -->
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-md-6 pb-3">
                        <div class="content h-100 hightlight-content-wrapper position-relative" style="background-image: url('/img/{{$plano->img}}')">
                            <div class="position-absolute text-wrapper d-flex align-items-center">
                            <p><div class="i-wrapper"><p><i class="fas fa-book"></i></p></div><span>{{strtoupper($plano->nome)}}</span></p>
                            </div>  
                        </div>       
                    </div>

                    @switch ($plano->id)
                        @case(8)
                        <div class="col-md-6">
                            <div class="content">
                                <p><div class="i-wrapper"><p><i class="fas fa-thumbtack"></i></p></div><span>Conteúdo</span></p>
                                <hr><p><i class="fas fa-check"></i>Aulas ao Vivo (Segunda a Quinta).</p>
                                <p><i class="fas fa-check"></i>Carga Horária: 50 H.</p>
                                <p><i class="fas fa-check"></i>Diversas questões comentadas ao vivo para PM-AL.</p>
                                <p><i class="fas fa-check"></i>Aulas no turno noturno.</p>
                                <p><i class="fas fa-check"></i>Depois da aula ao vivo, você poderá assistir às aulas a qualquer momento e quantas vezes quiser, durante a validade do curso.</p>
                                <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <p><div class="i-wrapper"><p><i class="fas fa-thumbtack"></i></p></div><span>Disciplinas</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i>Português.</p>
                                <p><i class="fas fa-check"></i>Informática.</p>
                                <p><i class="fas fa-check"></i>Matemática.</p>
                                <p><i class="fas fa-check"></i>História.</p>
                                <p><i class="fas fa-check"></i>Geografia.</p>
                                <p><i class="fas fa-check"></i>Direito Constitucional.</p>
                                <p><i class="fas fa-check"></i>Direito administrativo .</p>
                                <p><i class="fas fa-check"></i>Direito Penal.</p>
                                <p><i class="fas fa-check"></i>Legislação da PM.</p>
                                <p><i class="fas fa-check"></i>Direitos Humanos.</p>
                                <p><i class="fas fa-check"></i>Direito Processual Penal.</p>
                                <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <p><div class="i-wrapper"><p><i class="fas fa-gift"></i></p></div><span>Bônus</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i>Análise do último edital.</p>
                                <p><i class="fas fa-check"></i>Aprenda a montar o seu Plano de Estudo.</p>
                                <p><i class="fas fa-check"></i>Cronograma de estudos e conteúdo programático.</p>
                                <br>
                            </div>
                            <div class="content" style="height: 214px;">
                                <p><div class="i-wrapper"><p><i class="fas fa-gift"></i></p></div><span>Bônus Extra</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i><strong>5 aulões ao vivo.</strong></p>
                            </div>
                        </div>
                        @break
                        @case(4)
                        <div class="col-md-6">
                            <div class="content">
                                <p><div class="i-wrapper"><p><i class="fas fa-thumbtack"></i></p></div><span>Conteúdo</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i>O curso tem aproximadamente 350 aulas com cerca de 30 minutos cada.</p>
                                <p><i class="fas fa-check"></i>Todo material utilizado nas aulas será disponibilizado em PDF para um melhor aproveitamento do curso.</p>
                                <p><i class="fas fa-check"></i>Cada aula tem em média de 20 a 30 minutos.</p>
                                <p><i class="fas fa-check"></i>Estudo direcionado para o Enem e Vestibulares.</p>
                                <p><i class="fas fa-check"></i>Assista as aulas quantas vezes quiser (ILIMITADO).</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 pb-3">
                                <div class="content h-100">
                                    <p><div class="i-wrapper"><p><i class="fas fa-info"></i></p></div><span>Sobre</span></p>
                                    <hr>
                                    <p><i class="fas fa-chevron-right"></i>Esse curso é para você que começou a estudar agora ou para você revisar todo o conteúdo já estudado.</p>
                                    <p><i class="fas fa-chevron-right"></i>Melhor equipe de professores da região.</p>
                                    <p><i class="fas fa-chevron-right"></i>O curso é 100% atualizado.</p>
                                    <p><i class="fas fa-chevron-right"></i>Esse curso tem duração até Novembro de 2021.</p>
                                </div>
                            </div>
                            <div class="col-md-6 pb-3">
                                <div class="content h-100">
                                    <p><div class="i-wrapper"><p><i class="fas fa-exclamation-triangle"></i></p></div><span>Observações</span></p>
                                    <hr>
                                    <p><i class="fas fa-chevron-right"></i>Os vídeos podem sofrer alterações no tempo devido à dinâmica do professor em sala, priorizando sempre o melhor andamento da matéria de forma que a carga horária final seja cumprida.</p>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(5)
                        <div class="col-md-6 pb-3">
                            <div class="content h-100">
                                <p><div class="i-wrapper"><p><i class="fas fa-thumbtack"></i></p></div><span>Conteúdo</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i>Aulas ao Vivo (Segunda a Sexta).</p>
                                <p><i class="fas fa-check"></i>Materiais de Apoio em PDF.</p>
                                <p><i class="fas fa-check"></i>Plano de Estudo.</p>
                                <p><i class="fas fa-check"></i>Conteúdos que ➕ caem no Enem.</p>
                                <p><i class="fas fa-check"></i>Orientações e dicas de estudos.</p>
                            </div>
                        </div>
                        <div class="row pb-3">
                            <div class="col-md-6">
                                <div class="content h-100">
                                    <p><div class="i-wrapper"><p><i class="fas fa-info"></i></p></div><span>Sobre</span></p>
                                    <hr>
                                    <p><i class="fas fa-chevron-right"></i>Esse curso conta com um equipe de professores experientes em Enem e Vestibulares.</p>
                                    <p><i class="fas fa-chevron-right"></i>Acompanhamento da Equipe LN durante todo o curso.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content h-100">
                                <p><div class="i-wrapper"><p><i class="fas fa-gift"></i></p></div><span>Bônus</span></p>
                                    <hr>
                                    <p><i class="fas fa-check"></i>Curso completo do ENEM com mais de 350 aulas com vídeos de todas as áreas.</p>
                                </div>
                            </div>
                        </div>
                        @break
                        @case(6)
                        <div class="col-md-6">
                            <div class="content">
                                <p><div class="i-wrapper"><p><i class="fas fa-thumbtack"></i></p></div><span>Conteúdo</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i>Aulas ao Vivo (Segunda a Sexta).</p>
                                <p><i class="fas fa-check"></i>Materiais de Apoio em PDF.</p>
                                <p><i class="fas fa-check"></i>Plano de Estudo.</p>
                                <p><i class="fas fa-check"></i>Conteúdos que ➕ caem no Enem.</p>
                                <p><i class="fas fa-check"></i>Plataforma Imaginei de Redação - duas redações corrigidas mensais.</p>
                                <p><i class="fas fa-check"></i>Simulados Enem TRIEduc Mensais ⭐ (Com análise dos resultados pelo TRI).</p>
                                <p><i class="fas fa-check"></i>Tira dúvidas por WhatsApp - Grupo de estudos.</p> 
                                <p><i class="fas fa-check"></i>Consultoria de maximização de performance e análise de resultados nos simulados.</p> 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <p><div class="i-wrapper"><p><i class="fas fa-info"></i></p></div><span>Sobre</span></p>
                                <hr>
                                <p><i class="fas fa-chevron-right"></i>Esse curso conta com um equipe de professores experientes em Enem e Vestibulares.</p>
                                <p><i class="fas fa-chevron-right"></i>Acompanhamento da Equipe LN durante todo o curso.</p>
                                <p><i class="fas fa-chevron-right"></i>Reunião com os pais e/ou responsáveis (com a direção e coordenação pedagógica).</p>
                            </div>
                        </div>
                        <div class="col-md-6 pb-3">
                            <div class="content h-100">
                                <p><div class="i-wrapper"><p><i class="fas fa-gift"></i></p></div><span>Bônus</span></p>
                                <hr>
                                <p><i class="fas fa-chevron-right"></i>Consultoria pedagógica com orientações e dicas de estudos.</p>
                                <p><i class="fas fa-chevron-right"></i>Orientação de estudo por área de conhecimento (Exatas, Humanas, Linguagens e Biológicas).</p> 
                                <p><i class="fas fa-chevron-right"></i>Apoio socioemocional (motivação + engajamento + autocontrole).</p>
                            </div>
                        </div>
                        <div class="col-md-6 pb-3">
                            <div class="content h-100">
                                <p><div class="i-wrapper"><p><i class="fas fa-gift"></i></p></div><span>Bônus Extra</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i>Curso completo do <strong>ENEM</strong> com mais de 350 aulas com vídeos de todas as áreas.</p>
                                <br>
                            </div>
                        </div>
                        @break
                        @case(7)
                        <div class="col-md-6">
                            <div class="content">
                                <p><div class="i-wrapper"><p><i class="fas fa-thumbtack"></i></p></div><span>Conteúdo</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i>Análise do último edital.</p>
                                <p><i class="fas fa-check"></i>Aprenda a montar o seu Plano de Estudo.</p>
                                <p><i class="fas fa-check"></i>Cronograma de estudos e conteúdo programático.</p>
                                <p><i class="fas fa-check"></i>Material disponibilizado em PDF.</p>
                                <p><i class="fas fa-check"></i>Mais de 300 aulas.</p>   
                                <p><i class="fas fa-check"></i>Cada aula em média 30 minutos.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <p><div class="i-wrapper"><p><i class="fas fa-info"></i></p></div><span>Sobre</span></p>
                                <hr>
                                <p><i class="fas fa-chevron-right"></i>Esse curso conta com estudo direcionado para o concurso da Polícia Militar de Alagoas.</p>
                                <p><i class="fas fa-chevron-right"></i>90 dias de acesso, assista as aulas quantas vezes quiser dentro do período de acesso.</p>
                            </div>
                        </div>
                        <div class="col-md-6 pb-3">
                            <div class="content h-100">
                                <p><div class="i-wrapper"><p><i class="fas fa-gift"></i></p></div><span>Bônus</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i>Mais de 30 aulas.</p>
                                <p><i class="fas fa-check"></i>Resolução de mais de 200 questões comentadas para PM-AL.</p>
                                <br>
                            </div>
                        </div>
                        @break
                        @case(9)
                        <div class="col-md-6">
                            <div class="content">
                                <p><div class="i-wrapper"><p><i class="fas fa-thumbtack"></i></p></div><span>Conteúdo</span></p>
                                <hr>
                                <p><i class="fas fa-check"></i>Cronograma de estudos e conteúdo programático.</p>
                                <p><i class="fas fa-check"></i>Material disponibilizado em PDF.</p>
                                <p><i class="fas fa-check"></i>Mais de 200 aulas.</p>
                                <p><i class="fas fa-check"></i>Cada aula em média 30 minutos.</p>
                                <p><i class="fas fa-check"></i>90 dias de acesso, assista as aulas quantas vezes quiser dentro do período de acesso.</p>
                            </div>
                        </div>
                        <div class="col-md-6 pb-3">
                            <div class="content h-100">
                                <p><div class="i-wrapper"><p><i class="fas fa-info"></i></p></div><span>Sobre</span></p>
                                <hr>
                                <p><i class="fas fa-chevron-right"></i>No Curso Básico para Concursos você estuda as cincos disciplinas que mais recorrentes em provas: Português, Matemática, Direito Administrativo, Direito Constitucional e Informática.</p>
                                <p><i class="fas fa-chevron-right"></i>Teoria aliada à prática em aulas que você pode assistir repetidas vezes até assimilar totalmente o conteúdo e com professores que fazem parte da equipe que mais aprova em concursos na região.</p>
                            </div>
                        </div>
                        @break                                           
                    @endswitch
                    <div class="col-md-6">
                        <div class="payment">
                            <p class="title">Formas de pagamento</p>
                            <hr>
                            <div class="item p-3 mb-1">
                                <p><div class="i-wrapper"><p><i class="fas fa-money-bill"></i></p></div><strong>À vista ou pix: </strong><span class="money">{{$plano->valor}}&nbsp</span></p>
                            </div>
                            <div class="item p-3 mb-1">
                                <p><div class="i-wrapper"><p><i class="fas fa-file-invoice-dollar"></p></i></div><strong>Boleto bancário:</strong><span> até @if($plano->id == 7) 6x @elseif ($plano->id == 9) 1x @else 12x @endif de <span class="money">
                                @if ($plano->id == 4)62,00
                                @elseif ($plano->id == 6)136,00
                                @elseif ($plano->id == 7)64,45
                                @elseif ($plano->id == 9)300,00
                                @else {{round($plano->valor/12,2)}}
                                @endif
                                &nbsp
                                </span></span></p>
                            </div>
                            <div class="item p-3 mb-1">
                                <p><div class="i-wrapper"><p><i class="fas fa-credit-card"></i></p></div><strong>Cartão:</strong><span> até 12x de <span class="money">
                                @if ($plano->id == 4)58,33
                                @elseif ($plano->id == 6)159,15
                                @elseif ($plano->id == 7)34,94
                                @elseif ($plano->id == 9)29,95
                                @else {{round($plano->valor/12,2)}}
                                @endif
                                &nbsp
                                </span></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('common.footer')
@include('build.scripts')

<script src="{{mix('js/curso.js')}}"></script>

@stop

