@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<link href="{{url('css/noticia.css')}}" rel="stylesheet">

<div class="section-noticia mb-5">

        @include('common.top-bar', ['title'=>"Novidades", 'subtitle'=>"Últimas notícias"])

        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="noticia-card mb-3">
                        <nav class="mb-5"  aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('noticias')}}">Notícias</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{mb_strimwidth($noticia->titulo, 0, 30, "...")}}</li>
                            </ol>
                        </nav>
                        <p class="title">{{$noticia->titulo}}</p>
                        <p class="subtitle"><i class="theme-primary-color fas fa-calendar-alt"></i><i> postado em {{date("d/m/Y", strtotime($noticia->data_publicacao))}}</i></p>
                        <div class="image d-flex justify-content-center mb-3">
                            <img src="https://aulasremotas.lnconcursos.com.br/admin/home/arquivos/{{$noticia->img}}" alt="eliene concursos - notícia"></img>
                        </div>
                        {!!'<p class="content">'!!}
                            {!!base64_decode($noticia->noticia)!!}
                        {!!'</p>'!!}
                    </div>
                </div>
                <div class="col-md-3 d-flex justify-content-center">
                    <div class="ultimas-noticias">
                        <p class="title">Notícias Recentes</p>
                        <hr class="theme-secondary-color mb-4">
                        @isset( $noticias )
                        @foreach ($noticias as $key=>$noticia)
                        <div class="noticia position-relative" data-id="{{$noticia->id}}">
                            <p class="title">{{mb_strimwidth($noticia->titulo, 0, 40, "...")}}</p>
                            <img src="https://aulasremotas.lnconcursos.com.br/admin/home/arquivos/{{$noticia->img}}" alt="eliene concursos - notícia"></img>
                            <a class="stretched-link" href="{{url('noticia') . '/' . $noticia->id}}"></a>
                        </div>
                        @endforeach
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('common.footer')
@include('build.scripts')
@stop