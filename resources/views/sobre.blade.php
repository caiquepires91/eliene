@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')



<div class="section-quem-somos mb-5">

    @include('common.top-bar', ['title'=>"Quem Somos", 'subtitle'=>"Nossa missão é a sua aprovação"])

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col-md-6">
                        <p class="section-text">O LN Cursos e Concursos surgiu em 2009, com o ideal de preparar os alunos da região para os concursos públicos da região nordeste, tendo registrado altíssimas taxas de crescimento nos últimos anos. Nosso propósito é transformar vidas por meio da educação. A empresa surgiu de uma iniciativa da empresária Eliene Moura que, soma mais de 20 anos de experiência no mercado de concursos públicos e pré-vestibular. O objetivo é democratizar o ensino de uma maneira nunca vista na nossa região, oferecendo ferramentas inovadoras e conteúdo educacional de excelência para todos que buscam um futuro melhor.</p>
                        <p class="section-text">O LN Cursos e Concursos oferece cursos nas modalidades online e presencial, apresentando mais comodidade, economia e ganho de tempo aos concurseiros que buscam um ensino de qualidade. Agora também ofertando a modalidade online aos alunos que não podem frequentar as aulas presenciais e não têm acesso a escolas preparatórias nas localidades onde residem.</p>
                        <p class="section-text">Afinal, desde o início de suas atividades, o LN Concursos apoia-se sobre a ideia de que a educação tem um compromisso com a realidade em constante mudança. Para tanto, opera de modo flexível e dinâmico, com os olhos postos no futuro. Faz da educação e do conhecimento dos alunos e professores o eixo de suas preocupações.</p>
                        <p class="section-text">Os professores do LN Cursos e Concursos são renomados profissionais que possuem experiência no ramo dos concursos públicos e vestibulares. A equipe pedagógica conta com anos de experiências em concursos e se preocupa em atender bem todas as exigências necessárias em cada certame.</p>
                    </div>
                    <div class="col-md-6">
                        <div id="carouselSobre" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active ">
                                    <div class="img-wrapper d-block w-100">
                                        <img src="{{url('img/cartoon.webp')}}" alt="eliene - professora cartoon">
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="img-wrapper d-block w-100">
                                        <img src="{{url('img/cartoon.webp')}}" alt="eliene - professora cartoon">
                                    </div>
                                </div>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselSobre" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselSobre" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section-quem-somos my-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <p class="section-title">Missão</p>
                <div class="row">
                    <div class="col-md-8">
                        <p class="section-text">Capacitar pessoas para aprovação em concursos públicos e vestibulares com excelência no serviço prestado.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section-quem-somos my-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <p class="section-title">Visão</p>
                <div class="row">
                    <div class="col-md-8">
                        <p class="section-text">Ser reconhecido como a instituição de ensino que mais aprova em concursos públicos na região. Oferecendo uma educação transformadora e de qualidade, formando profissionais qualificados para servir à sociedade.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('common.footer')
@include('build.scripts')
@stop
