@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')
@include('common.common')

<link href="{{url('css/contato.css')}}" rel="stylesheet">

<div class="section-contato mb-5">

    @include('common.top-bar', ['title'=>"Contato", 'subtitle'=>"Mande uma mensagem"])

    <div class="container">
        <div class="row">
            <div class="col">
               <!--  <p class="section-title">Contato</p>
                <p class="section-subtitle">Mande uma mensagem</p> -->
                <div class="row">
                    <div class="col-md-6">
                        <form id="form-send-message" class="needs-validation" action="{{url('send-message')}}" novalidate>
                            @csrf
                            <div class="input-group mb-3 has-validation position-relative">
                                <span class="input-group-text" id="name-input"><i class="fas fa-user"></i></span>
                                <input id="#nome" type="text" class="form-control" name="nome" placeholder="Nome" aria-label="Name" aria-describedby="name-input">
                                <div class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                            <div class="input-group mb-3 has-validation position-relative">
                                <span class="input-group-text" id="email-input"><i class="fas fa-envelope-open-text"></i></span>
                                <input id="#email" type="email" class="form-control" placeholder="E-mail" name="email" aria-label="Email" aria-describedby="email-input">
                                <div class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                            <div class="input-group mb-3 has-validation position-relative">
                                <span class="input-group-text" id="phone-input"><i class="fas fa-phone"></i></span>
                                <input id="#telefone" type="text" class="form-control" name="telefone" placeholder="Telefone" aria-label="Telefone" aria-describedby="phone-input">
                                <div class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                            <div class="input-group mb-3 has-validation position-relative">
                                <select id="#assunto" class="form-select has-validation position-relative" name="assunto" aria-label="selecionar assunto">
                                    <option class="theme-primary-color" selected>Assunto</option>
                                    <option value="Dúvida">Dúvida</option>
                                    <option value="Sugestão">Sugestão</option>
                                    <option value="Crítica">Crítica</option>
                                    <option value="Elogio">Elogio</option>
                                </select>
                                <div class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                            <div class="input-group mb-3 has-validation position-relative">
                                <span class="input-group-text" id="input-message"><i class="fas fa-file-signature"></i></span>
                                <textarea id="#mensagem" class="form-control" name="mensagem" aria-label="Mensagem" placeholder="Mensagem" rows=3></textarea>
                                <div class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                            <div class="mb-3 d-flex justify-content-center">
                                <button id="#send_message" type="submit" class="btn theme-primary-btn mb-3">Enviar Mensagem</button>
                            </div>
                        </form>                      
                    </div>
                    <div class="col-md-6">
                        <div class="maps mb-3">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3936.25267605029!2d-38.22796858460297!3d-9.399209400862086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x70930b04ea864dd%3A0xc6a499dd8b9a94d!2sLN%20Cursos%20e%20Concursos!5e0!3m2!1sen!2sbr!4v1618839237543!5m2!1sen!2sbr" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                        <div class="contatos row">
                                <div class="col-md-6">
                                    <a href="tel:753281-2285"><p><i class="fas fa-phone-alt theme-primary-color"></i>(75) 3281-2285</p></a>
                                    <a href="tel:753281-0654"><p><i class="fas fa-phone-alt theme-primary-color"></i>(75) 3281-0654</p></a>
                                    <a href="tel:7599112-5290"><p><i class="fab fa-whatsapp theme-primary-color" style="font-weight:bold"></i>(75) 99112-5290</p></a>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fas fa-map-marker-alt theme-primary-color"></i> Rua Fábio Monteiro Guerra, 10, Centro, Paulo Afonso/BA</p>
                                    <a href="mailto: atendimento.lnconcursos@gmail.com"><p><i class="fas fa-envelope-open-text theme-primary-color"></i> atendimento.lnconcursos@gmail.com</p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('common.footer')
@include('build.scripts')

<script src="{{mix('js/contato.js')}}"></script>

@stop