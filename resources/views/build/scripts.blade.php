
<!-- JQuery -->
<script type="text/javascript" src="{{url('assets/JQuery/jquery-3.6.0.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/JQuery/jquery.mask.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/JQuery/inputmask/jquery.inputmask.min.js')}}"></script>

<!-- <script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script> -->

<!-- Bootstrap -->
<script src="{{url('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/bootstrap/js/bootstrap3-typeahead.min.js')}}"></script>

<!-- Common-->
<script src="{{mix('js/common.js')}}"></script>