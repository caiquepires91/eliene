<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="content-language" content="pt-br">

    <title>Eliene - Cursos e Concursos</title>

    <!-- Bootstrap CSS  -->
    <link href="{{url('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Fontawesom CSS  -->
    <link href="{{url('assets/fontawesome/css/all.min.css')}}" rel="stylesheet">

    <!-- Fonts -->
    <link href="{{url('css/fonts.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Custom -->
    <link href="{{url('css/colors.css')}}" rel="stylesheet">
    <link href="{{url('css/common.css')}}" rel="stylesheet">

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
        html {
            scroll-behavior: smooth;
        }
    </style>
</head> 