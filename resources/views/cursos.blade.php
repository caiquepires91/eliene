@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<link href="{{url('css/cursos.css')}}" rel="stylesheet">

<div class="section-matricula mb-5">

    @include('common.top-bar', ['title'=>"Matricule-se", 'subtitle'=>"Nossos cursos"])

    <div class="container">
        <div class="row">
            <div class="col">
                <!-- <p class="section-title">Matricule-se</p> -->
                <div class="row justify-content-between">
                    <div class="col">
                        <!-- <p class="section-subtitle">Nossos cursos</p> -->
                    </div>
                    <div class="col">
                        <div class="d-flex flex-row-reverse">
                        </div>
                    </div>
                </div>
                <div class="row">
                    @isset($planos)
                    @foreach($planos as $key=>$plano)
                    <div class="col-md-4 mb-3 d-flex justify-content-center">
                        <div class="matricula-card">
                            <div class="header" style="background-image: url('/img/{{$plano->img}}');"></div>
                            <div class="p-3">
                                <div class="text">
                                    <p class="title">{{$plano->nome}}</p>
                                    <p class="subtitle">{{$plano->resumo}}</p>
                                </div>
                                <div class="row mt-3 info">
                                    <div class="col-6">
                                        <p><i class="fas fa-user-friends"></i> 50 vagas</p>
                                        <p><i class="fas fa-calendar-alt"></i> Início imediato</p>
                                    </div>
                                    <div class="col-6">
                                        <p><i class="fas fa-donate"></i> 12X R$<span class="money">{{round($plano->valor/12,2)}}<span></p>
                                        <p><i class="fas fa-clock"></i> 120h/aula</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-6">
                                        <ul class="users-list" style="display:none;">
                                            <li style="background:url('img/logo.png')"></li>
                                            <li style="background:url('img/logo.png')"></li>
                                            <li style="background:url('img/logo.png')"></li>
                                            <li style="background:url('img/logo.png')"></li>
                                        </ul>
                                    </div>
                                    <div class="col-12 d-flex justify-content-center">
                                        <a class="theme-secondary-square-btn  btn" role="button" href="{{url('cursos/'.$plano->id)}}" >Saiba mais</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
    
@include('common.footer')
@include('build.scripts')
@stop