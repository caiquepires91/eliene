@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<style>
#erro p {
    font-size: xxx-large;
    text-align: center;
}

#erro .row {
    margin-top: 3rem;
    margin-bottom: 4rem;
}

#erro .content {
    color: #a0a0a0;
    text-align: center;
    font-weight: 600;
}

</style>

<div class="container">
    <div  class="row">
        <div class="col">
            <p class="section-title">Erro!</p>
            <p class="section-subtitle">{{$subject}}</p>
        </div>
    </div>
    <div id="erro">
        <div class="row justify-content-center">
            <div class="mx-auto content">
                <p><i class="fas fa-times-circle"></i></p>
                <p>{{$message}}</p>
            </div>
        </div>
    </div>
</div>


@include('common.footer')
@include('build.scripts')