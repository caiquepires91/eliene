<style>

    #feedbackModal .modal-header {
        border:none;
    }

    #feedbackModal .modal-header button {
        color:white;
    }

    #feedbackModal .modal-footer {
        border:none;
        padding-bottom: 2rem;
    }

    #feedbackModal .modal-footer button {
        color: #b13838;
        font-weight: 700;
        border-radius: 2rem;
        width: 15rem;
        margin:auto;
    }

    #feedbackModal .modal-content {
        background: #0d6efd;
        color:white;
        border:none;
    }

    #feedbackModal .modal-body-wrapper {
        display:flex;
        flex-direction:column;
    }

    #feedbackModal .modal-body-wrapper h2 {
        text-align: center !important;
        font-size: xxx-large !important;
        font-family: 'Lufga' !important;
        font-weight: 200 !important;
    }

    #feedbackModal .modal-body {
        padding-top: 0rem;
        padding-right: 1rem;
        padding-bottom: 2rem;
        padding-left: 1rem;
    }

    #feedbackModal .modal-body .feedback {
        text-align: center;
        margin-bottom: 0;
        visibility:visible;
    }
</style>

<!-- Modal Pop-up -->
<div class="modal fade" id="feedbackModal" tabindex="-1" data-bs-backdrop="static" aria-labelledby="feedbackModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="feedbackModalLabel"></h5>
            <a role="button" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></a>
        </div>
        <div class="modal-body">
            <div class="modal-body-wrapper">
                <h2><i class="fas fa-grimace"></i></h2>
                <p class="feedback">Algo não está certo!</p>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-bs-dismiss="modal" aria-label="Close" class="btn btn-light">ok</button>
        </div>
    </div>
    </div>
</div>
<!-- /Modal pop-up -->