<link href="{{url('css/navbar.css')}}" rel="stylesheet">

<div class="main-wrapper">

	<div class="navbar-top-wrapper theme-primary-bg">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-md-1 col-3 p-2 d-flex justify-content-center">
				  <div class="media-icon-wrapper">
					<a href="https://pt-br.facebook.com/lnconcursos" target="_blank" class="link-light"><i class="fab fa-facebook-f"></i></a>
					<a href="https://www.instagram.com/ln_concursos" target="_blank" class="link-light"><i class="fab fa-instagram"></i></a>
					<a href="https://www.youtube.com/channel/UCZckxZ-D3v2MG7Gy_fB_pQw" target="_blank" class="link-light"><i class="fab fa-youtube"></i></a>
				  </div>
				</div>
				<div class="col-md-8 col-9 p-2">
					<p>Mais de 10 anos de experiência nos campos de concursos públicos e vestibulares, sempre inovando, e trazendo novas metodologias</p>
				</div>
			</div>
		</div>
	</div>

	<div class="navbar-wrapper">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container-fluid">
				  <a class="navbar-brand" href="{{url('/')}}">
					<div class="brand-wrapper">
						<h1 class="header theme-primary-color d-flex aling-items-center">
							<img src="{{url('img/main-logo.svg')}}" alt="eliene cursos e concursos" style="height:40px; width:auto;" class="d-inline-block align-text-top"/>
						</h1>
						<!-- <div class="brand-img-wrapper">
							<img src="{{url('img/logo.png')}}" alt="eliene cursos e concursos" class="d-inline-block align-text-top"/>
						</div>
						<div class="brand-text-wrapper">
							<h1 class="header theme-primary-color">
								<p class="title">eliene</p><p class="brand-subtitle">CURSOS E CONCURSOS</p>
							</h1>
						</div> -->
					</div>
				  </a>
				  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					  <li class="nav-item">
						<a class="nav-link {{ $page == 'home-page' ? 'active':'' }}" aria-current="page" href="{{url('/')}}">Início</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link {{ $page == 'sobre-page' ? 'active':'' }}" href="{{url('/sobre')}}">Sobre</a>
					  </li>
					  <li class="nav-item dropdown">
						<a class="nav-link  {{ $page == 'cursos-page' ? 'active':'' }} dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						  Cursos
						</a>
						<ul id="plano-dropdown" class="dropdown-menu" aria-labelledby="navbarDropdown">
							@isset($planos)
							@foreach($planos as $plano)
							<li><a class="dropdown-item" data-id="{{$plano->id}}" href="{{url('cursos/'.$plano->id)}}">{{$plano->nome}}</a></li>
							@endforeach
							@endisset
						  <li><hr class="dropdown-divider"></li>
						  <li><a class="dropdown-item {{ $page == 'cursos-page' ? 'active':'' }}" href="{{url('/cursos')}}">Todos</a></li>
						</ul>
					  </li>
					  <li class="nav-item">
						<a class="nav-link {{ $page == 'noticias-page' ? 'active':'' }}" href="{{url('/noticias')}}" tabindex="-1" aria-disabled="true">Notícias</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link {{ $page == 'contato-page' ? 'active':'' }}" href="{{url('/contato')}}" tabindex="-1" aria-disabled="true">Contato</a>
					  </li>
					  <li class="nav-item d-flex">
					  	@php
						  	$loggedIn = false;
							$rota = 'main.php';
							$admin = session('admin');
							if ($admin) {
								$userName = $admin->nome;
								$loggedIn = true;
								$rota = 'main_admin.php';
							}
							if (Auth::guard('admin')->check()) {
								$userName = Auth::guard('admin')->user()->nome;
								$loggedIn = true;
								$rota = 'main_admin.php';
							}
							else if (Auth::guard('web')->check()) {
								$userName = Auth::guard('web')->user()->name;
								$loggedIn = true;
							}
						@endphp

						@if ($loggedIn)
						<a class="nav-link " href="{{url('https://aulasremotas.lnconcursos.com.br/aluno/' . $rota)}}" tabindex="-1" aria-disabled="true"><div class="aluno-btn theme-secondary-bg"><i class="fas fa-tv"></i> {{$userName}}</div></a>
						<a id="logout-btn" class="nav-link ps-0" href="{{url('logout')}}" tabindex="-1" aria-disabled="true"><div class="aluno-btn theme-secondary-bg d-flex p-1"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Sair" style="width: 35px;"><i class="fas fa-sign-out-alt m-auto"></i></div></a>
						@else
						<a class="nav-link " href="{{url('#section-acesso')}}" tabindex="-1" aria-disabled="true"><div class="aluno-btn theme-secondary-bg"><i class="fas fa-tv"></i>  Área do Aluno</div></a>
						@endif
					  </li>
					</ul>
				  </div>
				</div>
			  </nav>
		</div>
	</div>
	
</div>