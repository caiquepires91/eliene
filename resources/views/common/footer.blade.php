<link href="{{url('css/rodape.css')}}" rel="stylesheet">

<div class="section-rodape">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="row rodape-content">
                    <div class="col-md-3">
                        <a href="{{url('/')}}">
                        <div class="brand d-flex d-md-block justify-content-center mb-3 poition-relative">
                            <img src="{{url('img/logo-rodape.svg')}}" alt="marca eliene cursos e concursos">
                        </div>
                        </a>
                        <p class="mb-4">O LN Cursos e Concursos está no mercado há mais de 10 anos, e somos referência em Paulo Afonso-BA e região. São mais 400 aprovados em concursos públicos e em vestibulares</p>
                        <p>
                            <div class="media-icon-wrapper">
                                <a href="https://pt-br.facebook.com/lnconcursos" target="_blank" class="link-warning"><i class="fab fa-facebook-f"></i></a>
                                <a href="https://www.instagram.com/ln_concursos" target="_blank" class="link-warning"><i class="fab fa-instagram"></i></a>
                                <a href="https://www.youtube.com/channel/UCZckxZ-D3v2MG7Gy_fB_pQw" target="_blank" class="link-warning"><i class="fab fa-youtube"></i></a>
                            </div>
                        </p>
                    </div>
                    <div class="col-md-3">
                        <div class="mb-4">
                            <p class="rodape-content-title">ACESSO RÁPIDO</p>
                            <a href="{{url('/')}}"><p><i class="fas fa-check theme-secondary-color"></i> Início</p></a>
                            <a href="{{url('/sobre')}}"><p><i class="fas fa-check theme-secondary-color"></i> Sobre</p></a>
                            <a href="https://aulasremotas.lnconcursos.com.br"><p><i class="fas fa-check theme-secondary-color"></i> Conheça nossa estrutura</p></a>
                            <a href="{{url('/cursos')}}"><p><i class="fas fa-check theme-secondary-color"></i> Cursos</p></a>
                            <a href="{{url('/noticias')}}"><p><i class="fas fa-check theme-secondary-color"></i> Notícias</p></a>
                            <a href="https://www.unicesumar.edu.br" target="_blank"><p><i class="fas fa-check theme-secondary-color"></i> Unicesumar</p></a>
                            <a href="{{url('/contato')}}"><p><i class="fas fa-check theme-secondary-color"></i> Contato</p></a>
                        </div>
                        <!-- <div class="mb-4">
                            <p class="rodape-content-title">PROCESSOS SELETIVOS</p>
                            <a href="{{url('/')}}"><p><i class="fas fa-check theme-secondary-color"></i> Bolsa de estudos</p></a>
                        </div> -->
                    </div>
                    <div class="col-md-3">
                        <div class="mb-4">
                            <p class="rodape-content-title">ENSINO MÉDIO</p>
                            <a href="{{url('/eja')}}"><p><i class="fas fa-check theme-secondary-color"></i> EJA</p></a>
                        </div>
                        <div class="mb-4">
                            <p class="rodape-content-title">PRÉ-VESTIBULAR</p>
                            <a href="{{url('/cursos/4')}}"><p><i class="fas fa-check theme-secondary-color"></i> Enem Online</p></a>
                            <!-- <a href="{{url('/cursos/5')}}"><p><i class="fas fa-check theme-secondary-color"></i> Enem Gold</p></a> -->
                            <a href="{{url('/cursos/6')}}"><p><i class="fas fa-check theme-secondary-color"></i> Enem Premium</p></a>
                        </div>
                        <div class="mb-4">
                            <p class="rodape-content-title">CONCURSOS</p>
                            <a href="{{url('/cursos/7')}}"><p><i class="fas fa-check theme-secondary-color"></i> Preparatório PM-AL</p></a>
                            <a href="{{url('/cursos/9')}}"><p><i class="fas fa-check theme-secondary-color"></i> Curso Básico</p></a>
                            <!-- <a href="{{url('/cursos/8')}}"><p><i class="fas fa-check theme-secondary-color"></i> Esquenta PM-AL</p></a> -->
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="mb-1">
                            <p class="rodape-content-title">CONTATO</p>
                            <div class="d-flex align-items-center"><i class="fas fa-phone-alt theme-secondary-color"></i><div><p>(75) 3281-2285</p><p>(75) 3281-0654</p><p>(75) 99112-5290 <i class="fab fa-whatsapp"></i></p></div></div>
                            <p><i class="fas fa-map-marker-alt theme-secondary-color"></i> Rua Fábio Monteiro Guerra, 10, Centro, Paulo Afonso/BA</p>
                            <p><i class="fas fa-envelope-open-text theme-secondary-color"></i> atendimento.lnconcursos@gmail.com</p>
                            <a class="theme-secondary-alt-btn btn mt-5" role="button" href="{{url('https://aulasremotas.lnconcursos.com.br/')}}"><i class="fas fa-tv"></i> Ambiente do Aluno</a>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row rodape-bottom align-items-center">
                    <div class="col-md-6">
                    <div class="col"></div>
                        <p>Copyright © 2021. LN Cursos e Concursos. Todos os direitos reservados</p>
                    </div>
                    <div class="col"></div>
                    <div class="col-md-2 d-flex justify-content-end align-self-end">
                        <a class="link-light" href="#termo" target="_blank"><p>Termos de uso | Política de Privacidade</p></a>
                    </div>
                    <div class="col-md-1 d-flex justify-content-end  align-self-end">
                        <p><span>Por </span><a href="https://re9agencia.com.br/" target="_blank"><span class="theme-secondary-color">Re9 Agência</span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>