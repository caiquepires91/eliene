<style>
    .top-bar {
        background: #003a70b8;
    }

    .top-bar iframe {
        border: none;
        position: absolute;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: -1;
    }
</style>

<div class="top-bar position-relative theme-primary-bg py-5 mb-5">
    <iframe src="{{url('/bg')}}" style="border:none;" title="Iframe Example">
    </iframe>
    <div class="container">
        <div class="row">
            <p class="section-title">{{$title}}</p>
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <p class="section-subtitle" style="color: white;">{{$subtitle}}</p>
                </div>
                <div class="col-md-6">
                    @isset ($leftContent)
                    {!! html_entity_decode($leftContent) !!}
                    @endisset
                </div> 
            </div>
        </div>
    </div>
</div>