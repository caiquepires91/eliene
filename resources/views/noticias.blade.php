@extends('build.master')
@section('content')
@include('build.header')
@include('common.navbar')

<link href="{{url('css/noticias.css')}}" rel="stylesheet">

<div class="section-noticias mb-5" data-url="{{url('/')}}">

    @include('common.top-bar', ['title'=>"Novidades", 'subtitle'=>"Últimas notícias",
    'leftContent'=> htmlentities("<div class='input-group mb-3'><input type='text' class='form-control typeahead' data-provide='typeahead' placeholder='Pesquisar...' aria-label='Pesquisar' aria-describedby='button-addon2'><button class='btn btn-outline-secondary' type='button' id='button-addon2'><i class='fas fa-search'></i></button></div>")
    ])
        
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="noticias-wrapper">
                        @isset($noticias)
                        @php
                            $size = count($noticias);
                        @endphp
                        @foreach($noticias as $key=>$noticia)
                            @if ($key == 0)
                            <div class="row d-none d-md-flex" style="margin-bottom:5rem;">
                                <div class="col-md-9">
                                    <div class="noticias-card first-noticia position-relative" style="background-image: url('https://aulasremotas.lnconcursos.com.br/admin/home/arquivos/{{$noticias[0]->img}}');">
                                        <div class="text-wrapper position-absolute">
                                            <p class="subtitle"><i class="fas fa-calendar-alt"></i><i> postado em {{date("d/m/Y", strtotime($noticias[0]->data_publicacao))}}</i></p>
                                            <p class="title mb-3">{{strip_tags(mb_strimwidth($noticias[0]->titulo, 0, 30, "..."))}}</p>
                                            <p class="content">{{strip_tags(mb_strimwidth(base64_decode($noticias[0]->noticia), 0, 100, "..."))}}</p>
                                        </div>
                                        <a class="stretched-link" href="{{url('/noticia') . '/' . $noticias[0]->id}}"></a>
                                    </div>
                                </div>
                            @if ($size < 2)
                            </div>
                            @endif
                            @elseif ($key == 1)
                                <div class="col-md-3">
                                    <div class="noticias-card side-noticia position-relative mb-2">
                                        <div class="text-wrapper">
                                            <p class="subtitle"><i class="fas fa-calendar-alt"></i><i> postado em {{date("d/m/Y", strtotime($noticias[1]->data_publicacao))}}</i></p>
                                            <p class="title mb-3">{{strip_tags(mb_strimwidth($noticias[1]->titulo, 0, 30, "..."))}}</p>
                                            <p class="content">{{strip_tags(mb_strimwidth(base64_decode($noticias[1]->noticia), 0, 130, "..."))}}</p>
                                        </div>
                                        <a class="stretched-link" href="{{url('/noticia') . '/' . $noticias[1]->id}}"></a>
                                    </div>
                            @if ($size < 3)
                                </div>
                            </div>
                            @endif
                            @elseif ($key == 2)
                                    <div class="noticias-card side-noticia position-relative">
                                        <div class="text-wrapper">
                                            <p class="subtitle"><i class="fas fa-calendar-alt"></i><i> postado em {{date("d/m/Y", strtotime($noticias[2]->data_publicacao))}}</i></p>
                                            <p class="title mb-3">{{strip_tags(mb_strimwidth($noticias[2]->titulo, 0, 30, "..."))}}</p>
                                            <p class="content">{{strip_tags(mb_strimwidth(base64_decode($noticias[2]->noticia), 0, 130, "..."))}}</p>
                                        </div>
                                        <a class="stretched-link" href="{{url('/noticia') . '/' . $noticias[2]->id}}"></a>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="noticias-card mb-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="header" style="background-image: url('https://aulasremotas.lnconcursos.com.br/admin/home/arquivos/{{$noticia->img}}');"></div>
                                    </div>
                                    <div class="col">
                                        <div class="p-1">
                                            <div class="text m-3">
                                                <p class="title">{{strip_tags(mb_strimwidth($noticia->titulo, 0, 170, "..."))}}</p>
                                                <p class="subtitle"><i class="fas fa-calendar-alt"></i><i> postado em {{date("d/m/Y", strtotime($noticia->data_publicacao))}}</i></p>
                                                <img class="d-md-none" src="https://aulasremotas.lnconcursos.com.br/admin/home/arquivos/{{$noticia->img}}" alt="eliene concursos - notícia"></img>
                                                <p class="content">{{strip_tags(mb_strimwidth(base64_decode($noticia->noticia), 0, 300, "..."))}}</p>
                                            </div>
                                            <div class="row p-3 noticias-rodape">
                                                
                                                <div class="col-6">
                                                    <a class="theme-primary-btn  btn" role="button" href="{{url('/noticia') . '/' . $noticia->id}}">Leia mais</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                        <div id="pagination" class="d-flex m-md-5 justify-content-center">{{$noticias->links('pagination::bootstrap-4')}}</div>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('common.footer')
@include('build.scripts')

<script src="{{mix('js/noticias.js')}}"></script>

@stop