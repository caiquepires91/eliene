/** validation **/
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isCpf(cpf) {
    exp = /\.|-/g;
    cpf = cpf.toString().replace(exp, "");
    var digitoDigitado = eval(cpf.charAt(9) + cpf.charAt(10));
    var soma1 = 0,
            soma2 = 0;
    var vlr = 11;
    for (i = 0; i < 9; i++) {
        soma1 += eval(cpf.charAt(i) * (vlr - 1));
        soma2 += eval(cpf.charAt(i) * vlr);
        vlr--;
    }
    soma1 = (((soma1 * 10) % 11) === 10 ? 0 : ((soma1 * 10) % 11));
    soma2 = (((soma2 + (2 * soma1)) * 10) % 11);
    if (cpf === "11111111111" || cpf === "22222222222" || cpf === "33333333333" || cpf === "44444444444" || cpf === "55555555555" || cpf === "66666666666" || cpf === "77777777777" || cpf === "88888888888" || cpf === "99999999999" || cpf === "00000000000") {
        var digitoGerado = null;
    } else {
        var digitoGerado = (soma1 * 10) + soma2;
    }
    if (digitoGerado !== digitoDigitado) {
        return false;
    }
    return true;
}

function isDate(dateString) {
    // First check for the pattern
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if(year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
}

function isInputValid(input) {
    var type = $(input).prop(type);
    switch(type) {
        case 'email':
            return isEmail($(input).val());
        case 'cpf':
            return isCpf($(input).val());
        case 'date':
            return isDate($(input).val());
        default:
    }
}

function isInViewport(element) {
    var rect = element.getBoundingClientRect();
    var html = document.documentElement;
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || html.clientHeight) &&
        rect.right <= (window.innerWidth || html.clientWidth)
    );
}

function throwCommonFB(success, msg) {
    var icon;
    if (success) {
        icon = '<i class="fas fa-laugh-beam"></i>';
        color = "#058a65";
    }
    else {
        icon = '<i class="fas fa-grimace"></i>';
        color = "#b13838";
    }
    $("#feedbackModal .modal-content").css('background', color);
    $("#feedbackModal h2").html(icon);
    $("#feedbackModal .feedback").html(msg);
    $("#feedbackModal").modal("show");
}

function truncate(input, length) {
    if (input.length > length) {
       return input.substring(0, length) + '...';
    }
    return input;
 };


/** /validation **/

/** on document ready **/
$(document).ready(documentReady());
function documentReady(params) {

    // $('.money').mask('#.##0,00', {reverse: true});

 /*    $('.money').inputmask('decimal', {
        radixPoint:".",
        groupSeparator: ",",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        placeholder: '0',
        rightAlign: false,
       
  }); */
    
   /*  $(".money").inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': '.',
            'autoGroup': true,
            'digits': 2,
            'radixPoint': ",",
            'digitsOptional': false,
            'allowMinus': false,
            'prefix': 'R$ ',
            'placeholder': ''
    });
 */
    /* $(".money").toLocaleString("pt-BR", {
        style: "currency",
        currency: "BRL",
        minimumFractionDigits: 2
    }); */


    //$(".money").html(convertToCurrency($(".money").html()));

    // function convertToCurrency(value) {
    //     return value.toLocaleString("pt-BR", {
    //         style: "currency",
    //         currency: "BRL",
    //         minimumFractionDigits: 2
    //     });
    // }
    //$('.money').mask('000.000.000.000.000,00', {reverse: true});
    $('.money').prepend("R$ ");
    $('input[name="cpf"]').mask('000.000.000-00', {reverse: true});
    $('input[name="telefone"]').mask('(00) 00000-0000');
    $('input[name="cep"]').mask('00000-000');
    $('.date').mask('00/00/0000');

    $('input').on('input', function() {
        $(this).removeClass('is-invalid');
        $(this).removeClass('is-valid');
    });
}
/** /on document ready **/