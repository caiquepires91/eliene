var BASE_URL = $(".section-noticias").data("url");
console.log("base url", BASE_URL);

function truncate(input, length) {
    if (input.length > length) {
    return input.substring(0, length) + '...';
    }
    return input;
};


$('.typeahead').typeahead({
    source:  function (query, process) {
        console.log(query);
        return $.get(BASE_URL + "/autocomplete", { str: query }, function (data) {
                console.log(data.data);
                var noticias_wrapper = $(".noticias-wrapper");
                noticias_wrapper.html("");
                console.log("data size", data.data.length);
                if (data.data.length == 0) {
                    var noticia = '<p class="empty-noticias">Ops! Não econtramos nenhuma notícia para "' + query + '". <i class="fas fa-sad-tear"></i></p>';
                    noticias_wrapper.html(noticia);
                } else {
                    data.data.forEach(item => {
                        console.log("item", item);
                        var noticia = '<div class="noticias-card mb-3">'+
                            '<div class="row">'+
                            '<div class="col-md-3">'+
                                    '<div class="header" style="background-image: url('+ BASE_URL + '/img/' + item.img + ');"></div>'+
                                '</div>'+
                            ' <div class="col">'+
                                    '<div class="p-1">'+
                                    ' <div class="text m-3">'+
                                            '<p class="title">' + truncate(item.titulo, 170) + '</p>'+
                                        '<p class="subtitle"><i class="fas fa-calendar-alt"></i><i> postado em ' + item.data_publicacao + '</i></p>' +
                                        '<img class="d-md-none" src="' + BASE_URL + '/img/' + item.img + '" alt="eliene concursos - notícia"></img>' +
                                            '<p class="content">' + truncate(item.noticia, 300) + '</p>' +
                                        '</div>' +
                                    '  <div class="row p-3 noticias-rodape">' +
                                        '  <div class="col-6">' +
                                            ' <a class="theme-primary-btn  btn" role="button" href="noticia/' + item.id + '">Leia mais</a>' +
                                        ' </div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                        ' </div>' +
                    ' </div>';

                    console.log("html", noticia);

                    noticias_wrapper.append(noticia);
                    });
                }
                //return process(data);
        });
    }
});