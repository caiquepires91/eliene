

$( document ).ready(function() {

    /** cadastro **/

    $("select[name='estado']").change(function() {
        //console.log("estado:", $("select[name='estados'] option:selected").val());
        $.post("{{url('cidades')}}", {
            "estado":$("select[name='estado'] option:selected").val(),
            "_token": "{{ csrf_token() }}"
        }, function(data){
            if (data.success) {
                loadCidades(data.cidades);
            } else {
                //console.log('erro ao carregar cidades.');
            }
        }).fail(function(){
            //console.log('erro ao carregar cidades.');
        });
    });

    $("#form-cadastro").submit(
        function (e) {
            e.preventDefault();
            e.stopPropagation();
            //console.log($("#signup-email").val());
            //console.log("ation: " + $(this).attr('action'));

            var nome = $("#form-cadastro input[name='nome'");
            var email = $("#form-cadastro input[name='email'");
            var cpf = $("#form-cadastro input[name='cpf'");
            var nascimento = $("#form-cadastro input[name='nascimento'");
            var estado = $("#form-cadastro input[name='estado'");
            var cidade = $("#form-cadastro input[name='cidade'");
            var cep = $("#form-cadastro input[name='cep'");
            var telefone = $("#form-cadastro input[name='telefone'");
            var senha = $("#form-cadastro input[name='senha'");
            var confirmaSenha = $("#form-cadastro input[name='confirma-senha'");
            var checkbox = $("#form-cadastro input[name='termo'");

            var cancel = false;
            var input = null;

            if (!checkbox.is(":checked"))
            {
                cancel = true;
                input = checkbox;
            }

            if (senha.val() != confirmaSenha.val()) {
                cancel = true;
                input = confirmaSenha;
            }

            if (senha.val() == "") {
                cancel = true;
                input = senha;
            }

            if (telefone.val() == "") {
                cancel = true;
                input = telefone;
            }

            if (cep.val() == "") {
                cancel = true;
                input = cep;
            }

            if (cidade.val() == "") {
                cancel = true;
                input = cidade;
            }

            if (estado.val() == "") {
                cancel = true;
                input = estado;
            }

            if (!isDate(nascimento.val())) {
                cancel = true;
                input = nascimento;
            }

            if (!isCpf(cpf.val())) {
                cancel = true;
                input = cpf;
            }

            if (!isEmail(email.val())) {
                cancel = true;
                input = email;
            }

            if (nome.val() == "") {
                cancel = true;
                input = nome;
            }

            var formSerialized = $(this).serializeArray();
        
            if (cancel) {
                input.addClass("is-invalid");
                input.focus();
                //console.log("cadastro cancelado " + input.val());
            } else {
                $.post($(this).attr('action'), formSerialized, function(data) {
                    console.log("data: " + data);
                    if (data.success) {
                        throwCadastroFB(data.msg);
                    } else {
                        throwCadastroFB(data.msg);
                    }
                })
                .fail(function() {
                    throwCadastroFB("Ops! Algo deu errado. Por favor, tente mais tarde.");
                });
            }

        }
    );
    
    /** /cadastro **/

    /** loigin **/

    $("#form-home-acesso").submit(
        function (e) {
            e.preventDefault();
            e.stopPropagation();
            //console.log($("#signup-email").val());
            //console.log("ation: " + $(this).attr('action'));

            //var email = $("#form-home-acesso input[name='email'");
            var cpf = $("#form-home-acesso input[name='cpf'");
            var senha = $("#form-home-acesso input[name='password'");

            var cancel = false;
            var input = null;

            if (senha.val() == "") {
                cancel = true;
                input = senha;
            }

            if (!isCpf(cpf.val())) {
                cancel = true;
                input = cpf;
            }

            /*   if (!isEmail(email.val())) {
                cancel = true;
                input = email;
            } */

            var formSerialized = $(this).serializeArray();
        
            if (cancel) {
                input.addClass("is-invalid");
                input.focus();
                //console.log("cadastro cancelado " + input.val());
            } else {
                $.post($(this).attr('action'), formSerialized, function(data) {
                    //console.log("data: ", data.user);
                    if (data.success) {
                        //console.log("senha post result", senha.val());
                        //alert("success");
                        loginToExternal(data.user, senha.val());
                        //window.location.href="{{url('/')}}";
                    } else {
                        throwCommonFB(false, data.msg);
                    }
                })
                .fail(function() {
                    //console.log("Ops! Algo deu errado. Por favor, tente mais tarde.");
                    throwCommonFB(false, "Ops! Algo deu errado. Por favor, tente mais tarde.");
                });
            }

        }
    );

    function loginToExternal(user, senha) {
        //console.log("senha", senha);
        //console.log("in login", user);
        var formData = new FormData();
        formData.append('cpf', user.cpf);
        formData.append('senha', senha);

        $.ajax({
                url: 'https://aulasremotas.lnconcursos.com.br/aluno/login.php',  
                dataType: 'json', 
                cache: false,
                xhrFields: {withCredentials: true},
                crossDomain: true,
                contentType: false,
                processData: false,
                data: formData,                         
                method: 'post',
        
            })
        .done(resp => {
            //console.log("ext. resp", resp);
            //alert("check");
            if(!resp['status']){
                throwCommonFB(false, resp['msg']);
            }
            else{
                if(resp['nivel'] == '3'){   
                    window.location.href='https://aulasremotas.lnconcursos.com.br/aluno/main.php'
                }else{
                    //console.log(resp['nivel']);
                    if(resp['nivel'] == '2'){
                        window.location.href='https://aulasremotas.lnconcursos.com.br/aluno/main_admin_professor.php'
                    }else{
                        window.location.href='https://aulasremotas.lnconcursos.com.br/aluno/main_admin.php'
                    }
                }
            }
        });
    }

    /** /login **/

    /** recuperar senha **/
    $("#form-recuperar-senha").submit(function(e) {
        e.preventDefault();
        e.stopPropagation();

        $("#recuperarSenhaModal button[type='submit']").html("Aguarde...");

        var email = $("#form-recuperar-senha input[name='email'");

        var cancel = false;
        var input = null;

        if (!isEmail(email.val())) {
            cancel = true;
            input = email;
        }

        var formSerialized = $(this).serializeArray();
    
        if (cancel) {
            input.addClass("is-invalid");
            input.focus();
            $("#recuperarSenhaModal button[type='submit']").html("Redefinir");
            //console.log("cadastro cancelado " + input.val());
        } else {
            $.post($(this).attr('action'), formSerialized, function(data) {
                //console.log("data: " + data);
                if (data.success) {
                    $("#recuperarSenhaModal .feedback").html(data.msg);
                    $("#recuperarSenhaModal .feedback").css("color", "green");
                    $("#recuperarSenhaModal .feedback").fadeIn(function(){
                        $(this).delay(2500).fadeOut();
                    });
                } else {
                    $("#recuperarSenhaModal .feedback").html(data.msg);
                    $("#recuperarSenhaModal .feedback").css("color", "red");
                    $("#recuperarSenhaModal .feedback").fadeIn(function(){
                        $(this).delay(2500).fadeOut();
                    });
                }
            })
            .fail(function() {
                $("#recuperarSenhaModal .feedback").html("Ops! Algo deu errado. Tente mais tarde.");
                $("#recuperarSenhaModal .feedback").css("color", "red");
                $("#recuperarSenhaModal .feedback").fadeIn(function(){
                    $(this).delay(2500).fadeOut();
                });
            })
            .done(function(){
                $("#recuperarSenhaModal button[type='submit']").html("Redefinir");
            });
        }
    });
    /** /recuperar senha **/

    /** galeria */
    $(".galeria-card").click(function() {
        var id = $(this).data("id");
        var base_url = $(".main-banner").data('url');
        //console.log("maind url", base_url);
        window.location.href = base_url + '/galeria/' + id;
    })
    /** /galeria */

    
    document.addEventListener('scroll', function (event) {

            /** quem somos */
        var quem_somos_el = document.querySelector('.section-quem-somos .btn');
        if (isInViewport(quem_somos_el)) {
            $('.section-quem-somos #slider').addClass("slide-in");
        }
        /** /quem somos */

        /** diferenciais */
        var diferenciais_el = document.querySelector('.section-diferenciais .card-subtitle');
        if (isInViewport(diferenciais_el)) {
            //console.log("is in the viewport");
            var classes = document.getElementsByClassName("menu__container");
            $(classes[0]).addClass("menu__container_animate");
            setTimeout(function() {$(classes[1]).addClass( "menu__container_animate")}, 200);
            setTimeout(function() {$(classes[2]).addClass( "menu__container_animate")}, 300);
            setTimeout(function() {$(classes[3]).addClass( "menu__container_animate")}, 400);
        }
        /** diferenciais */

        
        var aprovados_el = document.querySelector('.section-aprovados .square');
        if (isInViewport(aprovados_el)) {
            //console.log("is in the viewport square");
            $(".section-aprovados .tilt-line").addClass("tilt-line-width");
        }
    }, true /*Capture event*/);
    
});

function isInViewport(element) {
    var rect = element.getBoundingClientRect();
    //console.log(rect);
    var html = document.documentElement;
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || html.clientHeight) &&
        rect.right <= (window.innerWidth || html.clientWidth)
    );
}

function throwCommonFB(success, msg) {
    var icon;
    if (success) {
        icon = '<i class="fas fa-laugh-beam"></i>';
        color = "#058a65";
    }
    else {
        icon = '<i class="fas fa-grimace"></i>';
        color = "#b13838";
    }
    $("#feedbackModal .modal-content").css('background', color);
    $("#feedbackModal h2").html(icon);
    $("#feedbackModal .feedback").html(msg);
    $("#feedbackModal").modal("show");
}

function throwCadastroFB(msg) {
    $("#cadastro-feedback").html(msg);
    $("#cadastro-feedback").fadeIn(function() {
        $(this).delay(2500).fadeOut();
    });
}

/** validation **/
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isCpf(cpf) {
    exp = /\.|-/g;
    cpf = cpf.toString().replace(exp, "");
    var digitoDigitado = eval(cpf.charAt(9) + cpf.charAt(10));
    var soma1 = 0,
            soma2 = 0;
    var vlr = 11;
    for (i = 0; i < 9; i++) {
        soma1 += eval(cpf.charAt(i) * (vlr - 1));
        soma2 += eval(cpf.charAt(i) * vlr);
        vlr--;
    }
    soma1 = (((soma1 * 10) % 11) === 10 ? 0 : ((soma1 * 10) % 11));
    soma2 = (((soma2 + (2 * soma1)) * 10) % 11);
    if (cpf === "11111111111" || cpf === "22222222222" || cpf === "33333333333" || cpf === "44444444444" || cpf === "55555555555" || cpf === "66666666666" || cpf === "77777777777" || cpf === "88888888888" || cpf === "99999999999" || cpf === "00000000000") {
        var digitoGerado = null;
    } else {
        var digitoGerado = (soma1 * 10) + soma2;
    }
    if (digitoGerado !== digitoDigitado) {
        return false;
    }
    return true;
}

function isDate(dateString) {
    // First check for the pattern
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if(year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
}
/** /validation **/

function loadCidades(cidades) {
    $selectEl = $("select[name='cidade']");
    $selectEl.empty();
    $selectEl.append('<option selected>Cidade</option>');
    $.each(cidades, function(index, item) {
        var option = document.createElement("option");
        option.setAttribute('data-cep',item.cep);
        option.text = item.nome;
        option.value = item.cod_cidades;
        $selectEl.append(option);
    });
    $selectEl.prop('disabled', false);
}