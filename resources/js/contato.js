$("#form-send-message").submit(
    function (e) {
        e.preventDefault();
        e.stopPropagation();

        var email = $("#form-send-message input[name='email']");
        var nome = $("#form-send-message input[name='nome']");
        var assunto = $("#form-send-message input[name='assunto']");
        var telefone = $("#form-send-message input[name='telefone']");
        var mensagem = $("#form-send-message input[name='mensagem']");
        var button = $("#form-send-message button[type='submit']");

        button.html("Enviando...");

        var cancel = false;
        var input = null;

        if (mensagem.val() == "") {
            cancel = true;
            input = mensagem;
        }

        if (assunto.val() == "") {
            cancel = true;
            input = assunto;
        }

        if (telefone.val() == "") {
            cancel = true;
            input = telefone;
        }

        if (!isEmail(email.val())) {
            cancel = true;
            input = email;
        }

        var formSerialized = $(this).serializeArray();
    
        if (cancel) {
            input.addClass("is-invalid");
            input.focus();
            button.html("Enviar Mensagem");
        } else {
            $.post($(this).attr('action'), formSerialized, function(data) {
                console.log("data: ", data.msg);
                if (data.success) {
                    throwCommonFB(true, data.msg);
                } else {
                    throwCommonFB(false, data.msg);
                }
            })
            .fail(function() {
                console.log("Ops! Algo deu errado. Por favor, tente mais tarde.");
                throwCommonFB(false, "Ops! Algo deu errado. Por favor, tente mais tarde.");
            })
            .done(function(){
                button.html("Enviar Mensagem");
            });
        }

    }
);

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function throwCommonFB(success, msg) {
    var icon;
    if (success) {
        icon = '<i class="fas fa-laugh-beam"></i>';
        color = "#058a65";
    }
    else {
        icon = '<i class="fas fa-grimace"></i>';
        color = "#b13838";
    }
    $("#feedbackModal .modal-content").css('background', color);
    $("#feedbackModal h2").html(icon);
    $("#feedbackModal .feedback").html(msg);
    $("#feedbackModal").modal("show");
}