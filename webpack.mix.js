const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        require('tailwindcss'),
    ]).version();

mix.js('resources/js/common.js', 'public/js').version()
    .postCss('resources/css/common.css', 'public/css', [
        require('tailwindcss'),
    ]).version();

mix.postCss('resources/css/colors.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.postCss('resources/css/navbar.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.postCss('resources/css/rodape.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.js('resources/js/home.js', 'public/js')
.postCss('resources/css/home.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.js('resources/js/galeria.js', 'public/js')
.postCss('resources/css/galeria.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.postCss('resources/css/cursos.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.js('resources/js/curso.js', 'public/js')
.postCss('resources/css/curso.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.js('resources/js/noticias.js', 'public/js')
.postCss('resources/css/noticias.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.postCss('resources/css/noticia.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.js('resources/js/contato.js', 'public/js')
.postCss('resources/css/contato.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.js('resources/js/album.js', 'public/js')
.postCss('resources/css/album.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.postCss('resources/css/eja.css', 'public/css',[
    require('tailwindcss'),
]).version();

mix.sass('resources/css/bg.scss', 'public/css',[
    require('tailwindcss'),
]).version();

mix.sass('resources/css/flip.scss', 'public/css',[
    require('tailwindcss'),
]).version();