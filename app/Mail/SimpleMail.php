<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SimpleMail extends Mailable
{
    use Queueable, SerializesModels;

    public $sender;
    public $receiver;
    public $subject = "(no subject)";
    public $header;
    public $view;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($from, $to, $subject, $data, $view)
    {
        $this->sender = $from;
        $this->receiver = $to;
        $this->subject = $subject;
        $this->view = $view;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $from_user = "=?UTF-8?B?".base64_encode($this->receiver)."?=";
        $subject = "=?UTF-8?B?".base64_encode($this->subject)."?=";

        $this->header = "From: $from_user <no-reply@biologiaaprova.com.br>\r\n".  
        "MIME-Version: 1.0" . "\r\n" . 
        "Content-type: text/html; charset=UTF-8" . "\r\n";

        $this->withSwiftMessage(function ($message) {
            $message->getHeaders()->addTextHeader($this->header, 'true');
        });
        
        return $this->from($this->sender)->subject($this->subject)->view($this->view)->with($this->data);
    }
}
