<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class CustomEloquentProvider extends EloquentUserProvider {

     /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        if ($credentials['password'])
            $plain = $credentials['password'];
        else
            $plain = $credentials['senha'];
        // insert md5 check here
        if (md5($plain) ==  $user->getAuthPassword()) return true;
        else return false;

        return $this->hasher->check($plain, $user->getAuthPassword());
    }

}