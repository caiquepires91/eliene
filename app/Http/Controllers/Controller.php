<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $GUARD_DEFAULT = 'web';
    protected $GUARD_ADMIN = 'admin';
    protected $GUARD_ALUNO = 'aluno';

    protected function formatCPF($cpf){
        $cpf = trim($cpf);
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace(",", "", $cpf);
        $cpf = str_replace("-", "", $cpf);
        $cpf = str_replace("/", "", $cpf);
        return $cpf;
    }

    protected function formatDate($date) {
        $collection = Str::of($date)->split('/[\s\/]+/');
        $day = $collection->values()->get(0);
        $month = $collection->values()->get(1);
        $year = $collection->values()->get(2);
        return $year . '-' . $month . '-' . $day;
    }

    protected function generateToken() {
        return md5(rand(1, 10) . microtime());
    }
}
