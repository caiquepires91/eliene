<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;
use App\Http\Controllers\PlanoController;

class NoticiaController extends Controller
{
    protected $page;
    protected $noticias;
    protected $planos;

    public function __construct(PlanoController $planoController) {
        $this->page = "noticias-page";
        $this->noticias = Noticia::whereDeletado(0)->orderBy('id', 'desc')->paginate(6);
        $this->planos = $planoController->all();
    }

    public function index() {
        return view('noticias', [
            'page'=>$this->page,
            'planos'=>$this->planos,
            'noticias'=>$this->noticias
            ]);
    }

    public function show($id) {
        $noticia = Noticia::find($id);
        if ($noticia) return view('noticias.noticia', ['page'=>$this->page,  'planos'=>$this->planos, 'noticia'=>$noticia, 'noticias'=>$this->noticias]);
        else back();
    }

    public function autocomplete(Request $request) {
        $query = '%'.$request->str.'%';
        $noticias = Noticia::where("titulo", 'like', $query)->orderBy('id', 'desc')->paginate(6);
        return response()->json($noticias);
    }
}
