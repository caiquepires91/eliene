<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;
use App\Models\Cidade;
use App\Models\Estado;
use App\Models\Album;
use App\Models\Depoimento;
use App\Http\Controllers\PlanoController;

class HomeController extends Controller
{
    protected $page;
    protected $noticias;
    protected $planos;
    protected $estados;
    protected $albums;
    protected $depoimentos;

    public function __construct(PlanoController $planoController) {
        $this->page = "home-page";
        $this->noticias = Noticia::whereDeletado(0)->orderBy('id','desc')->limit(3)->get();
        $this->planos = $planoController->all();
        $this->estados = Estado::all();
        $this->albums = Album::whereDeletado(0)->limit(8)->get();
        $this->depoimentos = Depoimento::whereDeletado(0)->get();
    }

    public function index() {
        return view('home', [
            'page'=>$this->page,
            'planos'=>$this->planos,
            'noticias'=>$this->noticias,
            'estados'=>$this->estados,
            'albums'=>$this->albums,
            'depoimentos'=>$this->depoimentos
            ]);
    }
}
