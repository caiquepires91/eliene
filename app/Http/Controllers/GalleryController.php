<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Album;
use App\Models\Picture;

class GalleryController extends Controller
{
    protected $page;
    protected $albums;
    protected $planos;

    public function __construct(PlanoController $planoController) {
        $this->page = "home-page";
        $this->albums = Album::whereDeletado(0)->paginate(10);
        $this->planos = $planoController->all();
    }

    public function index() {
        return view('galeria.galeria', ['page'=>$this->page,  'planos'=>$this->planos, 'albums'=>$this->albums]);
    }

    public function show($id) {
        $album = Album::find($id);
        $pics = Picture::whereAlbum($album->id)->whereDeletado(0)->paginate(10);
        if ($pics) return view('galeria.album', ['page'=>$this->page,  'planos'=>$this->planos, 'album'=>$album, 'pics'=>$pics]);
        else back();
    }
}
