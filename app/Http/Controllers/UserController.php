<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Aluno;
use App\Models\Admin;
use App\Models\RecuperarSenha;
use Illuminate\Support\Facades\Hash;
use DateTime;
use Mail;
use App\Mail\SimpleMail;

class UserController extends Controller
{

    public function goToLoginPage() {
        $action = 'login';
        return redirect('/')->with($action);
    }

 /*    public function authenticate(Request $request)  {
        $email = $request->input('email');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password, 'deletado' => 0])) {
            $request->session()->regenerate();
            return ['success'=>true];
        }

        return ['success'=>false, 'msg'=>'e-mail ou senha incorretos'];
    } */

    public function authenticate(Request $request)  {
        $cpf = $this->formatCPF($request->input('cpf'));
        $password = $request->input('password');

        $admin = Admin::whereCpf($cpf)->whereAtivo(1)->first();
        if ($admin) {
            return $this->authUserById($request, $admin, $password, $this->GUARD_ADMIN);
        }

        $user = User::whereCpf($cpf)->whereDeletado(0)->whereAtivo(1)->first();
        if ($user) {
           return $this->authUserById($request, $user, $password, $this->GUARD_DEFAULT);
        } else {
            $aluno = Aluno::whereCpf($cpf)->whereAtivo(1)->first();
            if ($aluno) {
                $user = new User();
                $user->name = $aluno->nome;
                $user->email = $aluno->email;
                $user->cpf = $aluno->cpf;
                $user->ativo = $aluno->ativo;
                $user->password = $aluno->senha;
                $user->save();
                if ($user) {
                    return $this->authUserById($request, $user, $password, $this->GUARD_DEFAULT);
                }
            }    
        }

        return ['success'=>false, 'msg'=>'CPF ou senha incorretos'];
    }

    private function authUserById($request, $user, $password, $guard) {
        $senha_md5 = md5($password);
        if ($user->password)
            $senha = $user->password;
        else
            $senha = $user->senha;

        //return ["guard"=>$guard, "senha"=>$senha, "senha_md5"=>$senha_md5, "user"=>$user];

        if ($senha == $senha_md5) {
            if ($guard == $this->GUARD_ADMIN) {
                $request->session()->put('admin', $user);
                $request->session()->regenerate();
                return ['success'=>true, 'user'=>$user];
            }

            if (Auth::guard($guard)->loginUsingId($user->id)) {
                $request->session()->regenerate();
                return ['success'=>true, 'user'=>$user];
            } else {
                return ['success'=>false, 'msg'=>'CPF ou senha incorretos'];
            }
        }

        return ['success'=>false, 'msg'=>'CPF ou senha incorretos'];
    }

    public function logout(Request $request) {
        Auth::logout();
        $request->session()->flush();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }

    public function checkEmail(Request $request) {
        if (User::whereEmail($request->input('email'))->count() > 0) {
            return ['success'=>false, 'msg'=>'e-mail já cadastrado.'];
        } else {
            return redirect('/cadastrar');
        }
    }

    public function cadastrar(Request $request) {
        
        if (User::whereEmail($request->input('email'))->count() > 0 || User::whereCpf($request->input('cpf'))->count() > 0) {
            $response = ['success'=>false, 'msg'=>'CPF ou endereço de e-mail já foi cadastrado.'];
        } else {
            $user = new User();
            $user->name = $request->input('nome');
            $user->email = $request->input('email');
            $user->cpf = $this->formatCPF($request->input('cpf'));
            $user->nascimento = $this->formatDate($request->input('nascimento'));
            $user->estado = $request->input('estado');
            $user->cidade = $request->input('cidade');
            $user->cep = $request->input('cep');
            $user->telefone = $request->input('telefone');
            $user->password = md5($request->input('senha'));
            $user->save();

            if ($user) {
                //$aluno = $this->cadastrarAluno($user);
                $response = ['success'=>true, 'user'=>$user, 'aluno'=>$aluno, 'msg'=>'É isso aí, cadastro realizado.'];
            } else {
                $response = ['success'=>false, 'msg'=>'Ops! Algo deu errado.'];
            }
        }

        return $response;
    }

    private function cadastrarAluno($user) {
        $aluno = new Aluno();
        $aluno->nome = $user->name;
        $aluno->email = $user->email;
        $aluno->cpf = $user->cpf;
        $aluno->senha = $user->password;
        return $aluno->save();
    }

    public function recuperarSenha(Request $request) {
        $email = $request->input("email");
        $user = User::whereEmail($email)->latest('id')->first();
        if ($user != null) {
            $token = $this->generateToken();
            $recuperarSenha = new RecuperarSenha();
            $recuperarSenha->idUser = $user->id;
            $recuperarSenha->codigo = $token;
            $recuperarSenha->email = $user->email;
            $recuperarSenha->horaEnvio = new DateTime('now');
            $recuperarSenha->usado = false;
            $recuperarSenha->save();

            $data = [
                'nome'=>$user->nome,
                'url'=> url('/verificar-token') . "/" . $token
            ];

           // return $data;
    
            $to = $user->email;
            $subject = "Recuperar senha";
            $from = "atendimento.lnconcursos@gmail.com";
            $view = 'emails.recuperar_senha';
    
            Mail::to($to)->send(new SimpleMail($from, $to, $subject, $data, $view));

            $result = ['success'=>true, 'data'=>$data, 'msg'=>'Enviamos um link para o e-mail: ' . $user->email];
        } else {
            $result = ['success'=>false, 'msg'=>'Nenhum usuário cadastrado para ' . $email];
        }
        return $result;
    }

    public function redefinirSenha(Request $request) {
        $senha = $request->input("nova_senha");
        $codigo = $request->input("token");

        $recuperarSenha = RecuperarSenha::whereCodigo($codigo)->whereUsado(false)->first();
        if ($recuperarSenha != null) {
            $user = User::find($recuperarSenha->idUser);
            if ($user != null) {
                $user->password = Hash::make($senha);
                $user->save();
                if ($user != null) {
                    $recuperarSenha->usado = true;
                    $recuperarSenha->save();
                    $result = ['success'=>true, 'user'=>$user, 'msg'=>'Senha redefinida com sucesso.'];
                } else {
                    $result = ['success'=>false, 'msg'=>"Ops! Algo deu errado. Por favor, tente mais tarde."];
                }
            } else {
                $result = ['success'=>false, 'msg'=>"Ops! Algo deu errado. Por favor, tente mais tarde."];
            }
        } else {
            $result = ['success'=>false, 'msg'=>"Ops! Algo deu errado. Por favor, tente mais tarde."];
        }

        return $result;
    }

    public function verificarToken($token) {
        $codigo = $token;
        $recuperarSenha = RecuperarSenha::whereCodigo($codigo)->first();
        if ($recuperarSenha != null) {
            if ($recuperarSenha->usado) {
                return view('common.erro', ['title' => 'Eliene Cursos e Concursos - redefinir senha', 'page'=>'erro', 'subject'=>'Redefinir senha', 'message'=>"Esse código já foi utilizado."]);
            } else {
                return view('senha.recuperar-senha', ['title' => 'Eliene Cursos e Concursos - redefinir senha', 'page'=>'redefinir-senha', 'token'=>$codigo]);
            }
        } else {
            return view('common.erro', ['title' => 'Eliene Cursos e Concursos - redefinir senha', 'page'=>'erro', 'subject'=>'Redefinir senha', 'message'=>"Código inválido."]);
        }
    }

    public function enviarMensagem(Request $request) {
        $nome = $request->input("nome");
        $email = $request->input("email");
        $assunto = $request->input("assunto");
        $telefone = $request->input("telefone");
        $mensagem = $request->input("mensagem");

        $order   = array('\r\n', '\n', '\r');
        $replace = '<br/>';
        $mensagem = str_replace($order, $replace, $mensagem);

        $data = [
            'nome'=>$nome,
            'email'=>$email,
            'assunto'=>$assunto,
            'telefone'=>$telefone,
            'mensagem'=>$mensagem
        ];

        $emails = ["atendimento.lnconcursos@gmail.com"];
        //$emails = ["caiquepires91@gmail.com"];

        $to = "atendimento.lnconcursos@gmail.com";
        //$to = "caiquepires91@gmail.com";
        $subject = $assunto;
        $from = $email;
        $view = 'emails.mensagem';

        Mail::to($emails)->send(new SimpleMail($from, $to, $subject, $data, $view));

        return ['success'=>true, 'msg'=>'Mensagem enviada!'];

    }

}
